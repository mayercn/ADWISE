package de.ipvs.fachstudie.graphpartitioning.evaluation;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.Map.Entry;

import de.ipvs.fachstudie.graphpartitioning.partitioner.Launcher;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.Memory;
import de.ipvs.fachstudie.graphpartitioning.util.Configuration;

public class FastEvaluation {

	private double replicationDegree; // no. replicas divided by no. vertices
	private double balance; // skewness of no. edges
	private EvaluationData data;
	private Memory memory;
	
	public FastEvaluation(long startTime, EvaluationData data, Memory memory) {
		this.data = data;
		this.memory = memory;
		this.replicationDegree = this.calculateReplicationDegree();
		this.balance = this.calculateBalance();
	}
	
	/**
	 * Returns the average deviation from the average partition load in %
	 * @return
	 */
	private double calculateBalance() {
		double numberOfPartitions = memory.getNumberOfPartitions();
		long numberOfEdges = 0;

		for (int pid=0; pid<numberOfPartitions; pid++) {
			numberOfEdges += memory.getEdgeCount(pid);
		}
		data.setNumberOfEdges(numberOfEdges);
		double avgPartitionSize = (double)numberOfEdges / numberOfPartitions;

		double sum = 0.0;

		for (int pid=0; pid<numberOfPartitions; pid++) {
			sum += Math.pow(memory.getEdgeCount(pid) - avgPartitionSize, 2);
		}
		double result = (Math.sqrt(sum / numberOfPartitions) / avgPartitionSize) * 100;

		return result;
	}

	private double calculateReplicationDegree() {
		return memory.getReplicationDegree();
	}

	/**
	 * Print evaluation on command line
	 */
	public void printOnConsole() {
		String text = "\n******************************start********************************\n" + "* Run on: "
				+ Launcher.getTimeStamp() + "\n" + "* Date: " + new Date(Launcher.getTimeStamp()) + "\n"
				+ "* Evaluation results:\n" + "*   graph: " + data.getGraphName() + "\n" + "*   strategy: "
				+ data.getStrategyName() + "\n" + "*   number of partitions: "
				+ data.getNumberOfPartitions() + "\n" + "*   used Window: "
				+ data.getUseWindow() + "\n" + "*   windowSize: " + Configuration.WINDOW_SIZE + "\n"
				+ "*   total number of vertices in graph: " + memory.getNoVertices() + "\n"
				+ "*   total number of edges in graph: " + data.getNumberOfEdges() + "\n" + "*   balance: "
				+ balance + "\n" + "*   replication degree: " + replicationDegree + "\n"
				+ "*   execution time: " + data.getExecutionTime() + "ms\n";

		text += "******************************end**********************************\n";
		System.out.println(text);
	}
	
	/**
	 * print evaluation into file
	 */
	public void printEvaluationToFile() {

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Configuration.OUTPUT_PATH
				+ "Evals.dat", true), "utf-8"))) {
			writer.write(data.getWindowSize() + "\t" 
					+ data.getNumberOfPartitions() + "\t"
//					+ Configuration.MAXIMAL_LATENCY + "\t"
//					+ Configuration.WINDOWSTRATEGY_ALPHA + "\t"
//					+ Configuration.WINDOWSTRATEGY_BETA + "\t"
//					+ Configuration.WINDOWSTRATEGY_GAMMA + "\t"
//					+ Configuration.WINDOWSTRATEGY_DELTA + "\t"
					+ replicationDegree + "\t"
					+ balance + "\t" + data.getExecutionTime() + "\n");
		} catch (Exception e) {
			System.out.println("ERROR: Couldn't write evaluation to file");
			e.printStackTrace();
		}

	}
	
	
}
