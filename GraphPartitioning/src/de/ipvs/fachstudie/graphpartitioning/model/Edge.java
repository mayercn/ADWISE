package de.ipvs.fachstudie.graphpartitioning.model;

import java.util.ArrayList;

/**
 * Models an edge of a graph. The edge can be either directed or undirected.
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 */
public class Edge implements Comparable<Edge> {
	private int u;
	private int v;

	/**
	 * @param u
	 *            source vertex
	 * @param v
	 *            destination vertex
	 */

	public Edge(int u, int v) {
		this.u = u;
		this.v = v;
	}

	/**
	 * 
	 * @return source vertex
	 */
	public int getFirstVertex() {
		return this.u;
	}

	/**
	 * 
	 * @return destination vertex
	 */
	public int getSecondVertex() {
		return this.v;
	}

	public String toString() {
		return this.getFirstVertex() + "\t" + this.getSecondVertex();
	}

	/**
	 * compares both vertices. Source and destination are considered
	 */
	@Override
	public int compareTo(Edge o) {
		int differenceFirstVertex = o.getFirstVertex() - this.getFirstVertex();
		if (differenceFirstVertex != 0) {
			return differenceFirstVertex;
		} else {
			return o.getSecondVertex() - this.getSecondVertex();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Edge))
			return false;
		Edge otherEdge = (Edge) other;
		return otherEdge.compareTo(this) == 0;
	}

	@Override
	public int hashCode() {
		return this.u + this.v;
	}

}
