package de.ipvs.fachstudie.graphpartitioning.model;

import gnu.trove.set.hash.TIntHashSet;

import java.util.HashSet;

import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.limitedMemoryAgingData.AgingDataEntry;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.limitedMemoryAgingData.DeleteListener;

/**
 * Models a partition. Is used in the memory of the algorithm. Can be used in a
 * limited memory.
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 */
public class Partition {

	private int id;
	private long edgeCount = 0;

	// Constructors
	public Partition(int id) {
		this.id = id;
	}

	// getter/setter
	public int getId() {
		return this.id;
	}

	public void addEdge(Edge e) {
		this.edgeCount++;
	}


	/**
	 * 
	 * @return the number of all edges, even if they aren't stored anymore
	 *         because of the limited memory.
	 */
	public long getEdgeCount() {
		return edgeCount;
	}

}
