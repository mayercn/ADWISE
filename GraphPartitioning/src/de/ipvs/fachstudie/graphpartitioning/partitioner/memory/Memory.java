package de.ipvs.fachstudie.graphpartitioning.partitioner.memory;

import java.util.ArrayList;
import java.util.HashSet;

import de.ipvs.fachstudie.graphpartitioning.evaluation.PartitionEvaluation;
import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import gnu.trove.set.hash.TIntHashSet;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 */
public interface Memory {
	/**
	 * Returns the number of partitions in the memory
	 * 
	 * @return
	 */
	public int getNumberOfPartitions();

	/**
	 * Stores the edge on the indicated partition.
	 * 
	 * @param e
	 * @param partitionId
	 */
	public void store(Edge e, int partitionId);

	/**
	 * Returns all the partition ids of the partitions where the given vertex is
	 * contained in.
	 * 
	 * @param id
	 * @return Set of ids.
	 */
	public TIntHashSet getPartitionIdsOfVertex(int id);

	/**
	 * Returns true if vertexID is replicated on partitionID, else false;
	 * @param vertexID
	 * @param partitionID
	 * @return
	 */
	public boolean contains(int vertexID, int partitionID);

	/**
	 * Prints the current content to the console.
	 */
	public void printMemoryContent();

//	/**
//	 * Returns a MemoryInfo object containing the following information about
//	 * the current state of the partitions: - partition containing the most
//	 * edges (id and edge count) - partition containing the least edges (id and
//	 * edge count) - average amount of edges on the partitions
//	 * 
//	 * @return MemoryInfo object
//	 */
//	public MemoryInfo getMemoryState();
//
//	/**
//	 * Returns a MemoryInfo object containing the following information about
//	 * the current state of the partitions whose ids are contained in the Set
//	 * <Integer> partitionIds: - partition containing the most edges (id and
//	 * edge count) - partition containing the least edges (id and edge count) -
//	 * average amount of edges on the partitions
//	 * 
//	 * 
//	 * @return MemoryInfo object
//	 */
//	public MemoryInfo getMemoryState(ArrayList<Integer> partitionIds);

//	/**
//	 * Returns the indicated partition in a wrapper class that provides only
//	 * getters so that data encapsulation is ensured.
//	 * 
//	 * @param id
//	 * @return PartitionInfo
//	 */
//	public PartitionInfo getPartitionInfo(int id);

	/**
	 * Randomly removes replicas from partitions. This might have the effect of
	 * constrained partitioning with lower replication degree.
	 * 
	 * @param percentage
	 *            in [0,1]
	 */
	public void removeRandomReplicas(double percentage);

	public int getLeastLoadedPartition();
	
	/**
	 * Returns the minimal load (i.e., number of edges) on any partition
	 * @return
	 */
	public long getMinLoad();
	
	/**
	 * Returns the maximal load (i.e., number of edges) on any partition
	 * @return
	 */
	public long getMaxLoad();
	
	/**
	 * Returns the average load (i.e., number of edges) on any partition
	 * @return
	 */
	public double getAvgLoad();

	/**
	 * Returns the edge count for the partition id
	 * @param pid
	 * @return
	 */
	public long getEdgeCount(int pid);

	/**
	 * Returns the replication degree, i.e., no. replicas divided by no. vertices
	 * @return
	 */
	public double getReplicationDegree();

	public long getNoVertices();

}
