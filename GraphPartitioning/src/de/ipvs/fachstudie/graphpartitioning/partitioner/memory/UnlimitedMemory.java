package de.ipvs.fachstudie.graphpartitioning.partitioner.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import de.ipvs.fachstudie.graphpartitioning.evaluation.PartitionEvaluation;
import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.model.Partition;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 */
public class UnlimitedMemory implements Memory {
	private ArrayList<Partition> partitions;
	private ArrayList<Integer> partitionIds;
	
	private HashMap<Integer,TIntHashSet> A; // assignment v_id -> Set of partitions
	

	public UnlimitedMemory(int partitionsCount) {
		this.partitions = new ArrayList<Partition>();
		this.partitionIds = new ArrayList<Integer>();
		this.A = new HashMap<Integer,TIntHashSet>(1000000);
		
		// init partitions
		Partition newPartition;
		for (int i = 0; i < partitionsCount; i++) {
			newPartition = new Partition(i);
			partitions.add(newPartition);
			partitionIds.add(newPartition.getId());
		}
		
	}

	public int getNumberOfPartitions() {
		return this.partitions.size();
	}

	/**
	 * Stores the edge on the indicated partition.
	 * 
	 * @param e
	 * @param partitionId
	 */
	@Override
	public void store(Edge e, int partitionId) {

		int u = e.getFirstVertex();
		int v = e.getSecondVertex();
		
		TIntHashSet partitions_u = A.get(u);
		TIntHashSet partitions_v = A.get(v);

		if (partitions_u == null) {
			partitions_u = new TIntHashSet();
			A.put(u, partitions_u);
		}
		partitions_u.add(partitionId);
		
		if (partitions_v == null) {
			partitions_v = new TIntHashSet();
			A.put(v, partitions_v);
		}
		partitions_v.add(partitionId);
		
		// store edge on the partition it was assigned to
		this.partitions.get(partitionId).addEdge(e);
	}

	/**
	 * Returns all the partition ids of the partitions where the given vertex is
	 * contained in.
	 * 
	 * @param id
	 * @return Set of ids.
	 */
	@Override
	public TIntHashSet getPartitionIdsOfVertex(int id) {
		
		return A.getOrDefault(id, new TIntHashSet());
	}


	/**
	 * Prints the current content of AgingDataContainer to the console.
	 */
	@Override
	public void printMemoryContent() {
//		for (Entry<Integer, Set<Integer>> entry : this.dataContainer.entrySet()) {
//			System.out.println(entry.getKey() + "\tis on partition(s):\t" + entry.getValue().toString());
//		}
	}
	
	

//	/**
//	 * Returns a MemoryInfo object containing the following information about
//	 * the current state of the partitions: - partition containing the most
//	 * edges (id and edge count) - partition containing the least edges (id and
//	 * edge count) - average amount of edges on the partitions
//	 * 
//	 * @return MemoryInfo object
//	 */
//	@Override
//	public MemoryInfo getMemoryState() {
//		return getMemoryState(partitionIds);
//	}
//
//	/**
//	 * Returns a MemoryInfo object containing the following information about
//	 * the current state of the partitions whose ids are contained in the Set
//	 * <Integer> partitionIds: - partition containing the most edges (id and
//	 * edge count) - partition containing the least edges (id and edge count) -
//	 * average amount of edges on the partitions
//	 * 
//	 * Of course our knowledge is limited since we can't keep all edges in
//	 * memory. So depending on the LimitedMemory's size the data is more or less
//	 * precise.
//	 * 
//	 * @return MemoryInfo object
//	 */
//	@Override
//	public MemoryInfo getMemoryState(ArrayList<Integer> partitionIds) {
//		Partition pMax = new Partition(Integer.MAX_VALUE);
//		long max = Long.MIN_VALUE;
//		Partition pMin = new Partition(Integer.MAX_VALUE);
//		long min = Long.MAX_VALUE;
//		long tempSize;
//		long totalEdges = 0;
//		double avgEdgeCount = 0.0;
//
//		// get all the partitions from by id
//		ArrayList<Partition> partitions = new ArrayList<Partition>();
//		for (int pID : partitionIds) {
//			partitions.add(this.partitions.get(pID));
//		}
//
//		// compute min/max/avg
//		for (Partition p : partitions) {
//			tempSize = p.getEdgeCount();
//			totalEdges += tempSize;
//			if (tempSize >= max) {
//				if (tempSize == max) {
//					if (p.getId() < pMax.getId()) {
//						max = tempSize;
//						pMax = p;
//					}
//				} else {
//					max = tempSize;
//					pMax = p;
//				}
//			}
//			if (tempSize <= min) {
//				if (tempSize == min) {
//					if (p.getId() < pMin.getId()) {
//						min = tempSize;
//						pMin = p;
//					}
//				} else {
//					min = tempSize;
//					pMin = p;
//				}
//			}
//		}
//		avgEdgeCount = (double) totalEdges / (double) partitions.size();
//		return new MemoryInfo(max, min, avgEdgeCount, pMax.getId(), pMin.getId());
//	}

//	/**
//	 * Returns a the indicated partition in a wrapper class that provides only
//	 * getters so that data encapsulation is ensured.
//	 * 
//	 * @param id
//	 * @return PartitionInfo
//	 */
//	@Override
//	public PartitionInfo getPartitionInfo(int id) {
//		return new PartitionInfo(this.partitions.get(id));
//	}

	/**
	 * Randomly removes replicas from partitions. This might have the effect of
	 * constrained partitioning with lower replication degree.
	 * 
	 * @param percentage
	 *            in [0,1]
	 */
	@Override
	public void removeRandomReplicas(double percentage) {
//		Random random = new Random();
//		int count = 0;
//		for (Integer v : dataContainer.keySet()) {
//			List<Integer> replicas = new ArrayList<Integer>(dataContainer.get(v));
//			int numberOfVerticesToRemove = (int) Math.round(replicas.size() * percentage);
//			for (int i = 0; i < numberOfVerticesToRemove; i++) {
//				int randIndex = random.nextInt(replicas.size());
//				replicas.remove(randIndex);
//				count++;
//			}
//			Set<Integer> newReplicas = new HashSet<Integer>(replicas);
//			dataContainer.put(v, newReplicas);
//		}
//		// System.out.println(count + " vertices removed!");
	}

	@Override
	public int getLeastLoadedPartition() {
		Partition leastLoaded = partitions.get(0);
		for (Partition partition : partitions) {
			if (partition.getEdgeCount()<leastLoaded.getEdgeCount()) {
				leastLoaded = partition;
			}
		}
		return leastLoaded.getId();
	}

	@Override
	public long getMinLoad() {
		long minLoad = Long.MAX_VALUE;
		for (int i=0; i<partitions.size(); i++) {
			long load = partitions.get(i).getEdgeCount();
			if (load<minLoad) {
				minLoad = load;
			}
		}
//		System.out.println("MinLoad: " + minLoad);
		return minLoad;
	}

	@Override
	public long getMaxLoad() {
		long maxLoad = 0;
		for (int i=0; i<partitions.size(); i++) {
			long load = partitions.get(i).getEdgeCount();
			if (load>maxLoad) {
				maxLoad = load;
			}
		}
//		System.out.println("MaxLoad: " + maxLoad);
		return maxLoad;
	}
	
	@Override
	public double getAvgLoad() {
		double avgLoad = 0;
		for (int i=0; i<partitions.size(); i++) {
			avgLoad += partitions.get(i).getEdgeCount();
		}
		return avgLoad / (double) partitions.size();
	}

	@Override
	public boolean contains(int vertexID, int partitionID) {
		TIntHashSet ps = A.get(vertexID);
		if (ps==null || !ps.contains(partitionID)) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public long getEdgeCount(int pid) {
		return partitions.get(pid).getEdgeCount();
	}

	@Override
	public double getReplicationDegree() {
		long noReplicas = 0;
		long noVertices = A.size();
		for (TIntHashSet value : A.values()) {
            noReplicas += value.size();
        }
		return (double)noReplicas / (double)noVertices;
	}

	@Override
	public long getNoVertices() {
		return A.size();
	}
	
}
