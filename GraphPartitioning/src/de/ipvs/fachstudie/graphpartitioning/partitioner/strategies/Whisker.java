package de.ipvs.fachstudie.graphpartitioning.partitioner.strategies;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.Memory;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.MemoryInfo;
import de.ipvs.fachstudie.graphpartitioning.util.NamesOfStrategies;
import gnu.trove.set.hash.TIntHashSet;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 *         The Degree algorithm as described in the S-PowerGraph Paper
 *
 */
public class Whisker implements SingleEdgePartitioningStrategy {
	private Edge e;
	private double maxEdgeCount;
	private double minEdgeCount;
	private long currentPartitionsEdgeCount;
	private double averageEdgesPerPartition;
	// allowed imbalance between max/average nodes on partitions
	private final double epsilon = 0.2;
	private Map<Integer, Double> degreeDistribution = new HashMap<Integer, Double>();
	private Memory memory;
	private long edgeCounter;
	private long assignedToDefault;
	private int defaultPartition;

	public Whisker(Memory memory) {
		this.memory = memory;
		this.edgeCounter = 0;
		this.assignedToDefault = 0;
	}

	/**
	 * Main method of the algorithm. This method implements the algorithm DEGREE
	 * as it is described in the S-PowerGraph paper. It returns for a given edge
	 * the optimal partition where to add the current edge in order to achieve
	 * optimal balance and replication.
	 */
	@Override
	public int getPartitionForEdge(Edge edge) {
		this.edgeCounter++;
		this.assignedToDefault++;
		//defaultPartition = (int) this.edgeCounter / 10000 % memory.getNumberOfPartitions();
		if (edgeCounter<50000) {
			return memory.getLeastLoadedPartition();
		}
		
		this.e = edge;
		double maxScore = 0.0;
		double currentScore = 0.0;
		double ratio;
		int bestPartitionId = 0;
		Set<Integer> bestOptions = new TreeSet<Integer>();
		boolean onlyBalance = false;


		ratio = ((double) maxEdgeCount / (double) averageEdgesPerPartition);
		onlyBalance = (ratio >= (1.0 + epsilon)) ? true : false;
		if (onlyBalance) {
			return memory.getLeastLoadedPartition();
		}
//			assignedToDefault++;
//			return defaultPartition;
//		}
		
		int partitionsCount = memory.getNumberOfPartitions();

		for (int partitionId = 0; partitionId < partitionsCount; partitionId++) {
//			if (onlyBalance) {
//				// only optimize balance
//				currentScore = getBalance(partitionId);
//			} else {
				// optimize overall score
				currentScore = getScoreOfPartition(partitionId);
//			}
//			System.out.print("\t" + Math.round(currentScore));
			if (currentScore > maxScore) {
				bestPartitionId = partitionId;
				maxScore = currentScore;
//				bestOptions.clear();
//				bestOptions.add(partitionId);
			}
//			else if (currentScore == maxScore) {
//				bestOptions.add(partitionId);
//			}
		}
//		System.out.println();
		// tie breaker necessary => choose randomly from the best partitions.
//		if (bestOptions.size() > 1) {
//			Object[] options = bestOptions.toArray();
//			bestPartitionId = (int) options[options.length / 2];
//		}
//		System.out.println("Max score: " + maxScore);
//		if (maxScore<2) {
//			return defaultPartition;
//		} 
		return bestPartitionId;
	}

	/**
	 * This method returns the score of the given partition in the current state
	 * of the iteration.
	 * 
	 * @param partitionId
	 * @return
	 */
	private double getScoreOfPartition(int partitionId) {
		double score = 0.0;
		int u = e.getFirstVertex();
		int v = e.getSecondVertex();
		double degreeU;
		double degreeV;

		TIntHashSet replicationsU = memory.getPartitionIdsOfVertex(u);
		TIntHashSet replicationsV = memory.getPartitionIdsOfVertex(v);

		degreeU = getDegreeEstimation(u);
		degreeV = getDegreeEstimation(v);

		if (replicationsU.contains(partitionId)) {
			// partition already contains a replication of vertex u
			score += 1.0;
		}
		if (replicationsV.contains(partitionId)) {
			// partition already contains a replication of vertex v
			score += 1.0;
		}
		if (degreeU <= degreeV) {
			if (replicationsU.contains(u)) {
				// smaller degree
//				score += 1.0;
				score += 0.2;
			}
		}
		if (degreeV <= degreeU) {
			if (replicationsV.contains(v)) {
				// smaller degree
//				score += 1.0;
				score += 0.2;
			}
		}
		score += getBalance(partitionId);
		return score;
	}

	/**
	 * Calculates the balance of the given partition. For the balance value we
	 * use the algorithm from the paper.
	 * 
	 * @param partitionId
	 * @return balance
	 */
	private double getBalance(int partitionId) {
		currentPartitionsEdgeCount = memory.getEdgeCount(partitionId);

		return ((double) (maxEdgeCount - currentPartitionsEdgeCount)) / ((double) (maxEdgeCount - minEdgeCount + 1.0));
	}


	/**
	 * Estimates the degree of a given node. The degree is for indirected edges.
	 * The method models a power-law distribution of vertex degrees.
	 * 
	 * @param vertex
	 * @return estimated degree of vertex
	 */
	private double getDegreeEstimation(int vertex) {
		double degree;

		if (degreeDistribution.containsKey(vertex)) {
			degree = degreeDistribution.get(vertex);
			degree++;
			degreeDistribution.put(vertex, degree);
		} else {
			degreeDistribution.put(vertex, 1.0);
			degree = 1.0;
		}
		return degree;
	}

	@Override
	public String toString() {
		return NamesOfStrategies.DEGREE.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		return new HashMap<String, Double>();

	}
}