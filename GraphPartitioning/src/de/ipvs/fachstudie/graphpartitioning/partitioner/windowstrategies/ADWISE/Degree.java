package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;

import de.ipvs.fachstudie.graphpartitioning.util.Configuration;

/**
 * Class to store and maintain the degree table during processing
 * @author mayercn
 *
 */
public class Degree {

	private static HashMap<Integer, Integer> degreeTable =
			new HashMap<Integer, Integer>();
	private static int maxDegree = 0;
	
	/**
	 * Returns the estimated degree of vertex with v_id
	 * @param v_id
	 */
	public static int deg(int v_id, int defaultDegree) {
		return degreeTable.getOrDefault(v_id, defaultDegree);
	}
	
	/**
	 * Overwrites the degree of vertex v_id with the value degree
	 * @param v_id
	 * @param degree
	 */
	public static void updateDegree(int v_id, int degree) {
		degreeTable.put(v_id, degree);
		if (degree>maxDegree) {
			maxDegree = degree;
		}
	}
	
	/**
	 * Returns the degree of the vertex with the largest degree that we have seen.
	 */
	public static int getMaxDegree() {
		return maxDegree;
	}
	
	public static void storeToFile() {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(Configuration.OUTPUT_PATH + "Degree.txt", true), "utf-8"))) {
			for (Integer key : degreeTable.keySet()) {
				writer.write(key + "\t" + degreeTable.get(key) + "\n");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
