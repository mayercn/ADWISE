package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

public class LatencyModel {
	
	/**
	 * Takes the total number of replicas after assigning the whole stream (expected),
	 * the number of vertices in the graph, and the (type of) algorithm
	 * and returns the latency of a single execution of the algorithm.
	 * @param totalReplicas
	 * @param numVertices
	 * @param algorithm
	 * @return
	 */
	public double expectedLatency(
			int totalReplicas, int numVertices, String algorithm) {
		return 0.0;
	}

}
