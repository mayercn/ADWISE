package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

import java.util.ArrayList;

import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.Memory;
import de.ipvs.fachstudie.graphpartitioning.util.Configuration;
import gnu.trove.set.hash.TIntHashSet;

public class WindowEdge_ADWISE extends Edge {
	public double[] scores;	// scores for all partitions
	public double maxScore = 0;

	public int[] replicaScores;
	public int maxRepScore;
	
	public WindowEdge_ADWISE(Edge e, int numberOfPartitions) {
		super(e.getFirstVertex(), e.getSecondVertex());
		replicaScores = new int[numberOfPartitions];
	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		// edges
		Edge otherEdge = (Edge) other;
		return otherEdge.compareTo(this) == 0;
	}
	
	/**
	 * Initializes the replication scores for each partition
	 * @param memory
	 * @param numberOfPartitions
	 */
	public void updateRepScores(Memory memory, int numberOfPartitions) {
		replicaScores = new int[numberOfPartitions];
		int u = this.getFirstVertex();
		int v = this.getSecondVertex();
		this.maxRepScore = 0;
		TIntHashSet partitions_u = memory.getPartitionIdsOfVertex(u);
		TIntHashSet partitions_v = memory.getPartitionIdsOfVertex(v);
		
		
		for (int p_id = 0; p_id<numberOfPartitions; p_id++) {
			if (replicaScores[p_id]<2) { // otherwise skip new score computation				
				// replication score
				int repScore = 0;
				if (partitions_u.contains(p_id)) {
					repScore++;
				}
				if (partitions_v.contains(p_id)) {
					repScore++;
				}
				replicaScores[p_id] = repScore;
				if (repScore>maxRepScore) {
					this.maxRepScore = repScore;
				}
			}
		}
	}
	
	public void updateScores(Memory memory, int numberOfPartitions,
			ArrayList<Double> balanceScores) {
		
		// we assume rep scores of all edges are already up to date
		
		scores = new double[numberOfPartitions];
		maxScore = -1;

		// specifity score (does not change for each partition)
		double specificityScore = getSpecifity();
		
		// calculate relative degree
		int v1 = this.getFirstVertex();
		int v2 = this.getSecondVertex();
		int deg_v1 = Degree.deg(v1,1);
		int deg_v2 = Degree.deg(v2,1);
//		double rel_deg_v1 = (double) deg_v1 / (double) (deg_v1+deg_v2);
//		double rel_deg_v2 = (double) deg_v2 / (double) (deg_v1+deg_v2);
		double rel_deg_v1 = (double) deg_v1 / (double) (2*Degree.getMaxDegree());
		double rel_deg_v2 = (double) deg_v2 / (double) (2*Degree.getMaxDegree());
		
		for (int p_id = 0; p_id<numberOfPartitions; p_id++) {
			
			// degree score (does not change for each partition
			double hdrfRepScore = hdrfRepScore(p_id, memory, v1, v2, rel_deg_v1, rel_deg_v2);
			
			// balance score (already multiplied with beta)
			double balanceScore = balanceScores.get(p_id);
			
			// degree score
//			double degreeScore = getDegreeScore(v1,v2);
			
			// total score
//			double score = hdrfRepScore + balanceScore * (1.8-specificityScore);
			double score = hdrfRepScore + balanceScore 
					+ Configuration.WINDOWSTRATEGY_GAMMA * specificityScore;
//					+ Configuration.WINDOWSTRATEGY_GAMMA * specificityScore;
//					+ Configuration.WINDOWSTRATEGY_DELTA * degreeScore;
			
//			int threshold = 2;
//			if (deg_v1<threshold || deg_v1<threshold) {
//				score = 2*hdrfRepScore;
//			}

//			System.out.println("Total score: " + score);
//			System.out.println("HDRF rep score: " + hdrfRepScore);
//			System.out.println("Bal score: " + balanceScore);
//			System.out.println("Specificity: " + specificityScore);
//			System.out.println("Degree: " + degreeScore);
//			System.out.println();
			scores[p_id] = score;
			if (score>maxScore) {
				maxScore = score;
			}
		}
	}

	public double getSpecifity(){
		float sum = 0.0f;
		for(int i = 0; i< replicaScores.length; i++){
			sum += Math.abs(this.maxRepScore - replicaScores[i]);	
		}
		sum = sum/(float) (2*(replicaScores.length-1));
		return sum;
	}
	
	/**
	 * The larger the summed degree of v1 and v2, the smaller the score, the
	 * more we focus on placing small degree edges optimally.
	 * @param v1
	 * @param v2
	 * @return
	 */
	public double getDegreeScore(int v1, int v2) {
		return 1-(double)(Degree.deg(v1, 1) + Degree.deg(v2, 1)) / (double)(Degree.getMaxDegree() * 2);
	}
	
	public double hdrfRepScore(int p_id, Memory memory,
			int v1, int v2, double rel_deg_v1, double rel_deg_v2) {
		double hdrfRepScore = 0;
//		System.out.println("Deg(v1): " + rel_deg_v1 + " deg(v2): " + rel_deg_v2);
		if (memory.contains(v1, p_id)) {
			// vertex v1 is replicated on p_id
			hdrfRepScore += 2-rel_deg_v1;
		}
		if (memory.contains(v2, p_id)) {
			// vertex v2 is replicated on p_id
			hdrfRepScore += 2-rel_deg_v2;
		}
		return hdrfRepScore;
	}
}
