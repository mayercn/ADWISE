package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.ipvs.fachstudie.graphpartitioning.util.Configuration;

public class WindowStatistics_ADWISE {
	
	
	// Window size -> exponential average latency
	double alpha = 0.9; // importance of last observation
	HashMap<Integer, Long> exponentialAverageLatency = 
			new HashMap<Integer, Long>(); // in ns
	
	HashMap<Integer, Long> summedLatency = new HashMap<Integer, Long>();
	HashMap<Integer, Long> n = new HashMap<Integer, Long>(); // number of observations
	
	
	
	/**
	 * One sample from assignment of a single edge:
	 * @param winSize - the current window size
	 * @param latency - the current latency using winSize edges in nanosecs
	 * @param repScore - the repScore from the assigned edge
	 */
	public void updateStatistics(int winSize, long latency) {
		
		// update exponential average latency
		long tmp_ea_lat = exponentialAverageLatency.getOrDefault(winSize, latency);
		
		// debug: window latencies for diff winSizes
//		if (!exponentialAverageLatency.containsKey(winSize)) {
//			System.out.println(this);
//		}
		
		exponentialAverageLatency.put(winSize, 
				Math.round(alpha*tmp_ea_lat + (1-alpha)*latency));
		
		summedLatency.put(winSize, summedLatency.getOrDefault(winSize, 0L) + latency);
		n.put(winSize, n.getOrDefault(winSize, 0L) + 1);
	}
	
	/**
	 * Returns the best window size, if we have to keep
	 * the latency bound maxLatency based on our observations.
	 * @param oldWindowSize
	 * @param maxLatency
	 * @return
	 */
	public int getWindowSize(int oldWindowSize, long maxLatency,
			int edgesProcessed, int candidateSize) {
		
		long maxLat = Math.round(maxLatency * 
				(1+(Math.random()-0.5)/5)); // randomize max latency a bit
		
		// initially, we invest more latency (cold start problem)
		maxLat = Math.round(maxLat * (1.5 - Math.min(1.0,
				(double)edgesProcessed /
				(double) Configuration.MINIMAL_GRAPH_SIZE)));
		
		// TODO: increase window only, if it lead to better score
		// TODO 2: Keep window size
		double latencyEstimation = getAverageLatency(oldWindowSize);
		
		// latency estimation has to be greater than all latencies of smaller windows
		for (Integer key : exponentialAverageLatency.keySet()) {
			if (key<oldWindowSize) {
				long minLat = exponentialAverageLatency.get(key);
				if (minLat>latencyEstimation) {
					latencyEstimation = minLat;
				}
			}
		}
		
//		System.out.println("Window: Avg=" + latencyEstimation + " " + maxLat);
		if (latencyEstimation<maxLat) {
			// we can only consider increasing window if we are not already too slow
			if (candidateSize<10) {
				return oldWindowSize * 2;
			} else if (latencyEstimation*2>maxLat) {
				// increasing the window size would actually risk to miss deadline
				return oldWindowSize;
			} else {
				// increase window size, if we still have latency capacity
//				System.out.println("Window: " + oldWindowSize + " --> " + oldWindowSize*2);
				return oldWindowSize * 2;
			}
		} else {
			// reduce window size as we don't have capacity
//			System.out.println("Window: " + oldWindowSize + " --> " + oldWindowSize/2);
			return Math.max(Configuration.MINIMAL_WINDOW_SIZE,oldWindowSize / 2);
		}
	}
	
	
	
	/**
	 * Return estimation of latency needed for a certain
	 * window size. Be defensive -> 30% Puffer
	 * @param winSize
	 * @return
	 */
	public double getAverageLatency(int winSize) {
//		return exponentialAverageLatency.get(winSize) * 1.5;
		return (double)this.summedLatency.get(winSize) / (double)this.n.get(winSize) * 1.3;
	}
	
	@Override
	public String toString() {
		String s = "\n";
		for (int key : this.exponentialAverageLatency.keySet()) {
			s += key + " -> " + this.exponentialAverageLatency.get(key) + "\n";
		}
		
		s += "\n";
		
		for (int key : this.summedLatency.keySet()) {
			s += key + " -> " + ((double)this.summedLatency.get(key) / (double)this.n.get(key)) + "\n";
		}
		
		s += "\n-----------------";
		
		return s;
	}
	
}
