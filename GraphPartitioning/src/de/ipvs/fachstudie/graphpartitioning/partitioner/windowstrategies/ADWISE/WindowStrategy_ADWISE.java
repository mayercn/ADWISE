package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

import de.ipvs.fachstudie.graphpartitioning.evaluation.PartitionEvaluation;
import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.Main;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.Memory;
import de.ipvs.fachstudie.graphpartitioning.partitioner.memory.MemoryInfo;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.AssignedEdge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.EdgeWindow;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.WindowBasedPartitioningStrategy;
import de.ipvs.fachstudie.graphpartitioning.util.Configuration;
import de.ipvs.fachstudie.graphpartitioning.util.NamesOfStrategies;

/**
 * 
 * 
 * @author Rieger, Lukas
 * @author Laich, Larissa
 * @author Geppert, Heiko
 * @author Mayer, Christian
 * 
 */
public class WindowStrategy_ADWISE implements WindowBasedPartitioningStrategy {


	public Memory memory;
	public int numberOfPartitions;
	private Window_ADWISE window;

	public HashMap<String, Double> parameters = new HashMap<String, Double>();
	public ArrayList<Double> balances = new ArrayList<Double>();
	
	public int assignmentCounter;
	long timestamp;
	
	// Adaptive Window
	private int counter;
	private WindowStatistics_ADWISE stat;
	private long deadline; // in ns

	public WindowStrategy_ADWISE(Memory memory) {
		// this(memory,windowSize);
		this.memory = memory;
		this.window = new Window_ADWISE(Configuration.WINDOW_SIZE + 1, this);
		this.numberOfPartitions = memory.getNumberOfPartitions();

		for (int i = 0; i < this.numberOfPartitions; i++) {
			this.balances.add(0.0d);
		}
		this.assignmentCounter = 0;
		this.counter = 0;
		this.stat = new WindowStatistics_ADWISE();
		this.deadline = System.nanoTime()
				+ (Configuration.MAXIMAL_LATENCY-1000) * 1000000; // 1 second puffer
	}

	@Override
	public AssignedEdge getPartitionForEdge() {

//		window.trackNumberOfOptions(assignmentCounter);
//		if (assignmentCounter<=10000 && assignmentCounter>10000-100) {
//			window.trackScores(assignmentCounter);
//		}
		
		// measure latency
		if (assignmentCounter==0) {
			timestamp = System.nanoTime();
		}
		long tmpLatency = System.nanoTime() - timestamp;
		this.timestamp = System.nanoTime();
		stat.updateStatistics(Configuration.WINDOW_SIZE, tmpLatency);
		
		// update dynamic beta parameter
		if (Configuration.DYNAMIC_BETA && assignmentCounter%50==0) {
			dynamicBeta();
		}

		// calculate new balancing scores
		// TODO: is this really good?
		if (assignmentCounter%30==0) {
			this.calculateBalances();
		}

		// get best edge in window
		WindowEdge_ADWISE bestEdge = window.getAndRemoveBestEdge();
		if (bestEdge==null) {
			return null;
		}
		double bestScore = 0;
		int bestPartition = 0;
		for (int p_id=0; p_id<numberOfPartitions; p_id++) {
			if (bestEdge.scores[p_id]>=bestScore) {
				bestScore = bestEdge.scores[p_id];
				bestPartition = p_id;
			}
		}
		
		AssignedEdge bestAssignment = new AssignedEdge(bestEdge, bestPartition);
		
		// update repScores of all edges that have changed && update candidates
		window.updateRepScores(bestEdge,bestPartition);

		// track variables for adaptive window
		this.assignmentCounter++;
		this.counter++;
		
		// adapt window size
		if (Configuration.ADAPTIVE 
				&& counter==Configuration.WINDOW_SIZE) {
			long maxLatency = calculateMaxLatencyPerEdgeAssignment();
//			System.out.println("Max latency (ns): " + maxLatency);
//			System.out.println("Latency needed (ns): " + tmpLatency);
//			System.out.println("Latency estimated (ns): " + stat.getAverageLatency(
//					Configuration.WINDOW_SIZE));
//			System.out.println("Old window size: " + Configuration.WINDOW_SIZE);
			if (maxLatency>0) {
				int newW = stat.getWindowSize(
						Configuration.WINDOW_SIZE, maxLatency,
						assignmentCounter, window.getCandidateSize());
				
				updateWindowSize(newW);
				
			} else {
				// deadline already missed: go go go!
				updateWindowSize(Configuration.MINIMAL_WINDOW_SIZE);
			}
			if (Configuration.WINDOW_SIZE>1)
//				System.out.println("New window size: " + Configuration.WINDOW_SIZE);
			counter = 0;
		}
		
		return bestAssignment;
	}
	
	

	/**
	 * Sets the new window size
	 * @param newWindowSize
	 */
	private void updateWindowSize(int newWindowSize) {
		Configuration.WINDOW_SIZE = newWindowSize;
		window.setSize(newWindowSize);
	}
	

	/**
	 * Returns the maximal latency allowed for assigning
	 * a single edge in nanoseconds.
	 * @return
	 */
	private long calculateMaxLatencyPerEdgeAssignment() {
		long edgesLeft = Configuration.MAXIMAL_GRAPH_SIZE - assignmentCounter;
		if (edgesLeft<1) {
			edgesLeft = 1;
		}
		double timeLeft = (double)(deadline-System.nanoTime()) 
				/ (double)edgesLeft; // nanosecs
		return Math.round(timeLeft);
	}

	private void calculateBalances() {

		double balScore;

		for (int pid = 0; pid < this.numberOfPartitions; pid++) {
			balScore = 0.0;
//			info = this.memory.getPartitionInfo(pid);
			// bal Score is between 0 and 1 (if beta is 1)--- + 0.05 avoid division by 0 
			// value is higher if partition should get edges (greater distance to maximum)
			long minLoad = this.memory.getMinLoad();
			long maxLoad = this.memory.getMaxLoad();
			balScore = (float) (Configuration.WINDOWSTRATEGY_BETA * 
					(maxLoad - this.memory.getEdgeCount(pid)) 
					/ (maxLoad - minLoad + 0.0001)); 
			this.balances.set(pid, balScore);
		}
	}


	@Override
	public EdgeWindow getWindow() {
		return this.window;
	}

	@Override
	public String toString() {
		return NamesOfStrategies.WINDOWSTRATEGY.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		return parameters;
	}

	/**
	 * Updates the beta parameter based on the imbalance of the partitions
	 */
	private void dynamicBeta() {

		// imbalance
		double min = memory.getMinLoad();
		double max = memory.getMaxLoad();
		double imbalance;
		if (max>0) {
			imbalance = (max-min)/max;
		} else {
			imbalance = 0;
		}
		//			System.out.println("Imbalance" + " " + imbalance);

		// tolerance as a function of the algorithm phase
		double tolerance = 1-(assignmentCounter
				/ (double) (Configuration.MINIMAL_GRAPH_SIZE));
		tolerance = Math.max(tolerance,0.1);

		//			tolerance = 0.05; // fraction 

		// adapt beta within interval [0.1,0.5]
		Configuration.WINDOWSTRATEGY_BETA += (imbalance - tolerance);
		double upper = 5;
//		double lower = 0.05;
		double lower = 0.6;
		if (Configuration.WINDOWSTRATEGY_BETA<lower) {
			Configuration.WINDOWSTRATEGY_BETA = lower;
		}
		if (Configuration.WINDOWSTRATEGY_BETA>upper) {
			Configuration.WINDOWSTRATEGY_BETA = upper;
		}
//		System.out.println(Configuration.WINDOWSTRATEGY_BETA);
	}

	public WindowStatistics_ADWISE getStat() {
		return stat;
	}
}
