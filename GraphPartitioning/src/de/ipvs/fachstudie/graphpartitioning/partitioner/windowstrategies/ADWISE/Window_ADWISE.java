package de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.ADWISE;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.EdgeWindow;
import de.ipvs.fachstudie.graphpartitioning.util.Configuration;
import de.ipvs.fachstudie.graphpartitioning.util.WriteToFile;

public class Window_ADWISE extends EdgeWindow {

	// the list of all edges in the window
	private List<WindowEdge_ADWISE> badEdges = new ArrayList<WindowEdge_ADWISE>();

	// only a few of them are candidates (those with high score)
	private ArrayList<WindowEdge_ADWISE> candidates = new ArrayList<WindowEdge_ADWISE>();
	private double exponentialAverageScore = 0;
	private double scoreThreshold = Configuration.WINDOWSTRATEGY_ALPHA;	// gets updated
	
	private WindowStrategy_ADWISE strategy;

	public Window_ADWISE(int size, WindowStrategy_ADWISE strategy) {
		super(size);
		this.strategy = strategy;
	}
	
	
	/**
	 * Update score threshold as exponential average
	 * of assigned edges.
	 * @param assignedScore
	 */
	private void updateScoreThreshold(double assignedScore) {
		exponentialAverageScore = 0.99 * exponentialAverageScore
				+ 0.01 * assignedScore;
		scoreThreshold = 0.1 + exponentialAverageScore;
		
//		System.out.println(candidates.size() + "\t" + badEdges.size() + "\t" + scoreThreshold);
//		System.out.println("Score threshold: " + scoreThreshold);
//		System.out.println("Best edge: " + assignedScore);
//		System.out.println("Candidate size: " + candidates.size());
//		System.out.println("Bad edges size: " + badEdges.size());
//		System.out.println("----------------");
	}
	
	@Override
	public boolean isEmpty() {
		return badEdges.isEmpty() && candidates.isEmpty();
	}

	@Override
	public boolean hasMoreEdges() {
		return isEmpty();
	}

	@Override
	public void setSize(int size) {
		super.setSize(size);
	}

	@Override
	public int getSize() {
		return super.getSize();
	}

	@Override
	public boolean hasCapacity() {
		return candidates.size()+badEdges.size()<Configuration.WINDOW_SIZE;
	}

	@Override
	public int getEdgeCount() {
		return super.getEdgeCount();
	}

	@Override
	public ArrayList<Edge> getEdges() {
		return super.getEdges();
	}

	@Override
	public boolean removeEdge(Edge e) {
		return true;
	}

	@Override
	public void addEdge(Edge e) {
		
		WindowEdge_ADWISE we = new WindowEdge_ADWISE(e, strategy.numberOfPartitions);
		we.updateRepScores(strategy.memory, strategy.numberOfPartitions);
		
		updateScore(we);
		
		// update scoreThreshold for lazy approach
		updateScoreThreshold(we.maxScore);
		
		updateDegreeDistribution(e);
		
		if (this.addToCandidates(we)) {
			if (!candidates.add(we)) {
				System.err.println("Could not add edge " + we);
				System.exit(-1);
			}
		} else {
			if (!badEdges.add(we)) {
				System.err.println("Could not add edge " + we);
				System.exit(-1);
			}
		}
	}
	
	/**
	 * Updates the degree for both incident vertices.
	 * @param e
	 */
	private void updateDegreeDistribution(Edge e) {
		
		// update degree distribution
		int v1 = e.getFirstVertex();
		int v2 = e.getSecondVertex();
		int degV1 = Degree.deg(v1, 0) + 1;
		int degV2 = Degree.deg(v2, 0) + 1;
		Degree.updateDegree(v1, degV1);
		Degree.updateDegree(v2, degV2);
	}

	/**
	 * Returns the edge with the highest score from the candidate
	 * set. If the candidate set is empty, the secondary set is used.
	 * @return
	 */
	public WindowEdge_ADWISE getAndRemoveBestEdge() {
		
//		System.out.println("Candidates: " + candidates.size());
//		System.out.println("      Threshold: " + scoreThreshold); 
		// ensure that candidates are not empty
		if (candidates.size()==0) {
			refreshCandidates();
		}
//		if (strategy.assignmentCounter%10==0) {
//			WriteToFile.writeln(
//					Configuration.OUTPUT_PATH + "windowInfo.dat", true, 
//					strategy.assignmentCounter + "\t" + candidates.size() 
//					+ "\t" + badEdges.size());
//		}
//		System.out.println(" Candidates: " + candidates.size() 
//				+ " Halde: " + badEdges.size());
		
		// get best edge from candidates
		WindowEdge_ADWISE bestEdge = null;
		double bestScore = -1;
		int index = 0;
		int i = 0;
		for (WindowEdge_ADWISE edge : candidates) {
			updateScore(edge);
			if (edge.maxScore>bestScore) {
				bestScore = edge.maxScore;
				index = i;
			}
			i++;
		}
		bestEdge = candidates.remove(index);
		return bestEdge;
	}
	
	/**
	 * Adds all edges from the badEdges to candidates
	 */
	private void refreshCandidates() {
		Iterator<WindowEdge_ADWISE> iter = badEdges.iterator();
		while (iter.hasNext()) {
			WindowEdge_ADWISE edge = iter.next();
			updateScore(edge);
			if (addToCandidates(edge)) {
				iter.remove();
				candidates.add(edge);
			}
		}
	}
	
	private void updateScore(WindowEdge_ADWISE we) {
		we.updateScores(strategy.memory, strategy.numberOfPartitions,
				strategy.balances);
	}
	

	/**
	 * Updates the repScores in all relevant window edges
	 * @param bestAssignment
	 */
	public void updateRepScores(WindowEdge_ADWISE we, int p_id) {
		int u = we.getFirstVertex();
		int v = we.getSecondVertex();
		boolean newReplicaU = !strategy.memory.contains(u, p_id);
		boolean newReplicaV = !strategy.memory.contains(v, p_id);

		if (newReplicaU) {
			increaseRepScore(candidates,true, u, p_id);
			increaseRepScore(badEdges,false,u, p_id);
		}
		if (newReplicaV) {
			increaseRepScore(candidates,true,v, p_id);
			increaseRepScore(badEdges, false,v, p_id);
		}
	}
	
	/**
	 * Increases the repScore of all vertices in the edge collection l
	 * that are equal vertexID.
	 * @param l
	 * @param vertexID
	 */
	private void increaseRepScore(Collection<WindowEdge_ADWISE> l,
			boolean candidateList, 
			int vertexID, int p_id) {
		Iterator<WindowEdge_ADWISE> iter = l.iterator();
		while (iter.hasNext()) {
			WindowEdge_ADWISE lwe = iter.next();
			int lwe_u = lwe.getFirstVertex();
			int lwe_v = lwe.getSecondVertex();
			if (vertexID==lwe_u || vertexID==lwe_v) {
				lwe.replicaScores[p_id]++;
				if (lwe.replicaScores[p_id]>lwe.maxRepScore) {
					lwe.maxRepScore = lwe.replicaScores[p_id];
				}
				if (!candidateList) {
					updateScore(lwe);
					if (addToCandidates(lwe)) {
						iter.remove();
						candidates.add(lwe);
					}
				}
			}
		}
	}
	
	private boolean addToCandidates(WindowEdge_ADWISE we) {
		if (!Configuration.LAZY) {
			return true;
		} else {
			// lazy window strategy is used
			if (candidates.isEmpty()) {
				return true;
			}
			if (we.maxRepScore>0
					) {
				return true;
			}
			return false;
		}
	}
	
	public void trackNumberOfOptions(int assignmentCounter) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(Configuration.OUTPUT_PATH + "No_options.txt", true), "utf-8"))) {
			// count number of edges with high score in window
			int c1 = 0;
			int c2 = 0;
			for (WindowEdge_ADWISE e : candidates) {
				for (int repScore : e.replicaScores) {
					c1 += repScore;
					c2 ++;
				}
			}
			writer.write(assignmentCounter + "\t" + c2 + "\t" + c1 + "\n");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void trackScores(int assignmentCounter) {
			try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(Configuration.OUTPUT_PATH 
							+ "Scores.txt", true), "utf-8"))) {
				for (WindowEdge_ADWISE we : candidates) {
					writer.write(we.getFirstVertex() + "\t" +
							we.getSecondVertex() + "\t" +
							assignmentCounter + "\t" + 
							we.maxScore + "\n");
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public int getCandidateSize() {
		return candidates.size();
	}
	
//	private void trackWindowScoreDistribution(int numberOfEdgesAssigned) {
//		String filename = Configuration.OUTPUT_PATH 
//				+ "windowScoreDistribution_" + Configuration.WINDOW_SIZE + ".txt";
//		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
//				new FileOutputStream(filename, true), "utf-8"))) {
//			writer.write(Configuration.WINDOW_SIZE + "\t"
//					+ numberOfEdgesAssigned + "\t"
//					+ summedAssignmentScore + "\t"
//					+ minAssignmentScore + "\t"
//					+ maxAssignmentScore + "\t"
//					+ summedLatency + "\t"
//					+ maxLatency + "\n"					
//					);
////			for (LazyWindowEdge we : badEdges) {
////				updateScore(we);
////				writer.write(we.maxScore + "\t" + we.maxRepScore + "\n");
////			}
//		} catch (Exception e) {
//			System.out.println("ERROR: Couldn't write evaluation to file");
//			e.printStackTrace();
//		}
//	}
	
}
