package de.ipvs.fachstudie.graphpartitioning.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.EdgeWindow;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 * 
 *         The FileReader reads the edges of a graph from a file. Edges must be
 *         given in the following notation: 2 4 for the edge (2,4)
 */
public class FileReader {
	private BufferedReader in;
	private long fileSize = 0;

	public FileReader(String inputFile) {
		try {
			File f = new File(inputFile);
			this.fileSize = f.length();
			this.in = new BufferedReader(new InputStreamReader(
					new FileInputStream(inputFile)),
					1024*1024*200);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

//	public boolean hasNextEdge() {
//
//		return scanner.hasNext();
//	}

	public long getFileSize() {
		return this.fileSize;
	}

	/**
	 * Christian: Reads the lines of the file and returns one by one all
	 * the lines that fit the notation defined above.
	 * 
	 * @return edge (or null if the eof is reached)
	 */
	public Edge getNextEdge() {
		try {
			String line = in.readLine();
			if (line==null || line=="") {
//				in.close();
//				System.out.println("EOF reached");
				return null;
			}
//			String[] s = line.split("\\s+");
			String[] s = fastSplit(line,2,' ','\t');
			int u = Integer.valueOf(s[0]);
			int v = Integer.valueOf(s[1]);
			Edge e = new Edge(u, v);
			return e;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	
	

	/**
	 * Fills the window with new edges and returns the edges added to the window
	 * 
	 * @param window
	 * @return
	 */
	public List<Edge> fillWindow(EdgeWindow window) {
		List<Edge> addedEdges = new ArrayList<Edge>();
		while (window.hasCapacity()) {
			Edge edge = getNextEdge();
			if (edge != null) {
				addedEdges.add(edge);
				window.addEdge(edge);
			} else {
				break;
			}
		}
		return addedEdges;
	}
	

	/**
	 * a faster splitting implementation 
	 * @param s string to be split
	 * @return
	 */
	private static String[] fastSplit(String s,
			int splitSize, char delimitter1, char delimitter2) {

		String[] result = new String[splitSize];
		int a = -1;
		int b = 0;

		for (int i = 0; i < splitSize; i++) {
			char charAtB = s.charAt(b);
			while (b < s.length() && s.charAt(b) != delimitter1 
					&& s.charAt(b) != delimitter2)
				b++;
			result[i] = s.substring(a+1, b);
			a = b;
			b++;		
		}

		return result;
	}

}
