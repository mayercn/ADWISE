package de.ipvs.fachstudie.graphpartitioning.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.partitioner.windowstrategies.EdgeWindow;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 * 
 *         The FileReader reads the edges of a graph from a file. Edges must be
 *         given in the following notation: 2 4 for the edge (2,4)
 */
public class FileReaderAsynch extends Thread {
	private BufferedReader in;
	private long fileSize = 0;
	private ArrayBlockingQueue<List<Edge>> edgeBatches;
	private int batchSize = 1000; // edges per blockingqueue put(...) batch
	private List<Edge> currentInBatch;
	private List<Edge> currentOutBatch;
	
	private boolean ready = false;

	public FileReaderAsynch(String inputFile) {
		try {
			File f = new File(inputFile);
			this.currentInBatch = new ArrayList<Edge>();
			this.currentOutBatch = new ArrayList<Edge>();
			this.fileSize = f.length();
			this.in = new BufferedReader(new InputStreamReader(
					new FileInputStream(inputFile)),
					1024*1024*200);
			this.edgeBatches = new ArrayBlockingQueue<List<Edge>>(1000);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!ready) {
				String line = in.readLine();
				if (line==null || line=="") {
					edgeBatches.put(currentInBatch);
					this.currentInBatch = new ArrayList<Edge>();
					ready = true;
				} else {
					String[] s = fastSplit(line,2,' ','\t');
					if (s!=null && line.charAt(0)!='%') {
						int u = Integer.valueOf(s[0]);
						int v = Integer.valueOf(s[1]);
						Edge e = new Edge(u, v);
						if (currentInBatch.size()<batchSize) {
							currentInBatch.add(e);
						} else {
							edgeBatches.put(currentInBatch);
							this.currentInBatch = new ArrayList<Edge>();
						}
					}
//					edges.put(e);
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public long getFileSize() {
		return this.fileSize;
	}

	/**
	 * Christian: Reads the lines of the file and returns one by one all
	 * the lines that fit the notation defined above.
	 * 
	 * @return edge (or null if the eof is reached)
	 */
	public Edge getNextEdge() {
		try {
			if (ready && currentOutBatch.isEmpty() && edgeBatches.isEmpty()) {
				return null;
			}
			if (currentOutBatch.isEmpty()) {
				currentOutBatch = edgeBatches.take();
			}
			Edge e = currentOutBatch.remove(0);
			return e;
			
			
//			if (ready && edges.isEmpty()) {
//				return null;
//			} else {
//				return edges.take();
//			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Fills the window with new edges and returns the edges added to the window
	 * 
	 * @param window
	 * @return
	 */
	public List<Edge> fillWindow(EdgeWindow window) {
		List<Edge> addedEdges = new ArrayList<Edge>();
		while (window.hasCapacity()) {
			Edge edge = getNextEdge();
			if (edge != null) {
				addedEdges.add(edge);
				window.addEdge(edge);
			} else {
				break;
			}
		}
		return addedEdges;
	}
	

	/**
	 * a faster splitting implementation 
	 * @param s string to be split
	 * @return
	 */
	private static String[] fastSplit(String s,
			int splitSize, char delimitter1, char delimitter2) {

		try {
			String[] result = new String[splitSize];
			int a = -1;
			int b = 0;

			for (int i = 0; i < splitSize; i++) {
				char charAtB = s.charAt(b);
				while (b < s.length() && s.charAt(b) != delimitter1 
						&& s.charAt(b) != delimitter2)
					b++;
				result[i] = s.substring(a+1, b);
				a = b;
				b++;		
			}

			return result;
		} catch(Exception e) {
			e.printStackTrace();
			System.err.println("There was some error splitting a line: ignored line " + s);
		}
		return null;
	}

}
