package de.ipvs.fachstudie.graphpartitioning.writer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import de.ipvs.fachstudie.graphpartitioning.model.Edge;
import de.ipvs.fachstudie.graphpartitioning.util.Configuration;
import de.ipvs.fachstudie.graphpartitioning.util.FileWriter;

/**
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 * PartitionWriter contains the buffer which edges to be written in one
 * step asynchronously.
 */
public class PartitionWriter extends Thread {

	// edge buffer for writing
	int partitionId;
	String fileName;
	private BufferedWriter out;
	private ArrayBlockingQueue<List<Edge>> buffer; // of edge batches
	int batchSize = 100;
	List<Edge> currentBatch;

	public PartitionWriter(int partitionId, String filePath, long timeStamp) {
		this.partitionId = partitionId;
		fileName = filePath + partitionId + ".txt";
		this.buffer = new ArrayBlockingQueue<List<Edge>>(1000);
		this.currentBatch = new ArrayList<Edge>();
		try {
			this.out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName, false), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			List<Edge> edgeBatch;
			while ((edgeBatch=buffer.take())!=null) {
				for (Edge e : edgeBatch) {
					out.write(e.toString() + "\n");
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void writeEdge(Edge e) {
		try {
			if (currentBatch.size()<batchSize) {
				currentBatch.add(e);
			} else {
				buffer.put(currentBatch);
				currentBatch = new ArrayList<Edge>();
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void flush() {
		try {
			buffer.put(currentBatch);
			currentBatch = new ArrayList<Edge>();
			this.out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
