package adwise;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;

import evaluation.GlobalEvaluation;
import memory.Memory;
import memory.UnlimitedMemory;
import model.GraphPartitioner;
import util.Config;
import util.NamesOfStrategies;
import util.Util;

public class Main_ADWISE {

	
	
	/**
	 * Executes the lazy adaptive window based partitioning algorithm
	 * @param
	 * 		args[0] - partitioner id
	 * 		args[1] - inFile
	 * 		args[2] - outPrefix
	 * 		args[3] - writeToFile (true/false)
	 * 		args[4] - strategyName (hashing/hdrf/degree/DBH/ADWISE)
	 * 		args[5] - number of partitions
	 * 		args[6] - minimal window size (ignored by non-window strategies)
	 * 		args[7] - maximal Latency (sec) for the whole partitioning task (ignored by non-window strategies)
	 * 		args[8] - dynamic beta? (true/false)
	 * 		args[9] - adaptive? (true/false)
	 * 		args[10]- lazy? (true/false)
	 *		args[11]- allowed partitions (e.g. 1,2,3)
	 */
	public static void main(String[] args) {
		
//		if (args.length>0) {
//			config.setParams(args);
//		} else {
//			config.setPartitioner_id(0);
//			config.setK(30);
//			config.setAdaptive(true);
//			config.setDynamicBeta(true);
//			config.setLazy(true);
//			config.setMaxLatency(200 * 1000);
//			config.setW(10);
//			config.setWrite_to_file(true);
//			config.setStrategyName(NamesOfStrategies.ADWISE.toString());
//			
//			config.setIn_filename("Z:\\GraphDataSet\\googleWeb\\10_chunks\\googleweb");
//			config.setOut_prefix("Z:\\GraphDataSet\\googleWeb\\10_chunks\\output\\" + 
//					config.getK() + "Part_" + config.getNumPartitioners() + "Thread\\");
//			
//		}

		Config config = new Config();
		if (args.length>0) {
			config.setParams(args);
		} else {
//			config.setAdaptive(true);
//			config.setAllowedPartitions(new HashSet<Integer>()
//					.addAll(new ArrayList<Integer>(new Integer[]{0,1,2,3,4,5,6,7,8})));
//			config.setBalancingWeight(balancingWeight);
//			config.setDynamicBeta(dynamicBeta);
//			config.setEpsilon(epsilon);
//			config.setGraphSize(graphSize);
//			config.setIn_filename(in_filename);
//			config.setK(k);
//			config.setLambda(lambda);
//			config.setLazy(lazy);
//			config.setMaxLatency(maxLatency);
//			config.setOut_prefix(out_prefix);
//			config.setPartitioner_id(partitioner_id);
//			config.setSpecificityWeight(specificityWeight);
//			config.setStrategyName(strategyName);
//			config.setW(w);
//			config.setWrite_to_file(write_to_file);
		}
		config.setGraphSize(Util.estimateNumEdges(config.getIn_filename() + config.getPartitioner_id()));
		GlobalEvaluation globalEval = new GlobalEvaluation(config);
		
		System.out.println("Start partitioner " + config.getPartitioner_id());
		System.out.println("Estimated #edges: " + config.getGraphSize());
		System.out.println("Allowed partitions: " + config.getAllowedPartitions());
		
		launch(config,globalEval);
		
//		for (int id=0; id<config.getNumPartitioners(); id++) {
//			
//			// each partitioner should have independent config access
//			Config configCopy = config.copy();
//			configCopy.setPartitioner_id(id);
//			configCopy.setIn_filename("" + config.getIn_filename() + "" + id);
//			
//			// start partitioner in new Thread -> as they are independent, no contention should arise
//			System.out.println("Start worker " + id);
//			Thread t = new Thread() {
//				public void run() {
//					launch(configCopy,globalEval);
//				}
//			};
//			t.start();
//		}
	}

	/**
	 * Launches a single partitioning process that reads one file and writes into k files.
	 * The partitioning process updates the parameter list.
	 * @param config
	 */
	public static void launch(Config config, GlobalEvaluation globalEval) {

		long startTime = System.currentTimeMillis();

		Memory memory = new UnlimitedMemory(config);

		GraphPartitioner partitioner = new GraphPartitioner(memory, config);

		System.out.println("[" + LocalTime.now() + "] computing: " + config.getStrategyName());

		// Partitioning
		partitioner.computePartitioning();

		System.out.println("\n[" + LocalTime.now() + "] evaluating: " + config.getStrategyName());

		// Update evaluation
		globalEval.updateMemory(memory);

		System.out.println("\n[" + LocalTime.now() + "] computation finished successfully.");

		long totalRuntime = System.currentTimeMillis() - startTime;
		System.out.println("Total Runtime: " + (totalRuntime / 1000) + "s");
		
		
	}
		
		
//			graph = "..\\graphs\\com-youtube_random.txt";
//			graph = "out.twitter";
//			graph = "brain.edges";
//						
//			graph = "..\\graphs\\wiki-Vote.txt";
//			graph = "..\\graphs\\twitter_random.txt";
//			graph = "..\\graphs\\www_random.txt";
//			graph = "www_combined";
//			graph = "..\\graphs\\facebook_combined.txt";
//			graph = "..\\graphs\\facebook_random.txt";
//			graph = "web-Google";
//			graph = "..\\graphs\\amazon_random.txt";
//			graph = "roadNet-CA";
//			graph = "soc-LiveJournal1";
//			graph = "wiki-Vote";
//			graph = "..\\graphs\\movielens100k_random.txt";
//			graph = "..\\graphs\\email-EuAll_combined.txt";
//			graph = "..\\graphs\\web-Stanford_random.txt";
			
//			Configuration.GRAPH = graph;
//			graph = "../graphs/" + graph + ".txt";
//			graph = "Z:\\GraphDataSet\\graphs\\chunks_webgoogle\\web-Google.txt";
			
//			graph = "Z:\\GraphDataSet\\twitter\\out.twitter";
//			graph ="Z:\\GraphDataSet\\BrainNetwork\\3chunks\\brain";
//			graph ="Z:\\GraphDataSet\\BrainNetwork\\1chunk\\brain";
//			graph ="Z:\\GraphDataSet\\googleWeb\\3_chunks\\googleWeb";
			
//			Configuration.ESTIMATED_GRAPH_SIZE = 165900000; // Brain
//			Configuration.ESTIMATED_GRAPH_SIZE = 1468365182; // TwitterL
//			Configuration.ESTIMATED_GRAPH_SIZE = 88234; // Facebook
//			Configuration.ESTIMATED_GRAPH_SIZE = 1497135; // WWW
//			Configuration.ESTIMATED_GRAPH_SIZE = 2420766; // Twitter
//			Configuration.ESTIMATED_GRAPH_SIZE = 5105039; // Web-Google
//			Configuration.ESTIMATED_GRAPH_SIZE = 3387393; // Amazon
//			Configuration.ESTIMATED_GRAPH_SIZE = 5533214; // Road-Net
//			Configuration.ESTIMATED_GRAPH_SIZE = 103689; // Wiki-Vote
//			Configuration.ESTIMATED_GRAPH_SIZE = 100000; // Movielens100k
//			Configuration.ESTIMATED_GRAPH_SIZE = 420045; // Email
//			Configuration.ESTIMATED_GRAPH_SIZE = 2312497; // Stanford Web
//			Configuration.ESTIMATED_GRAPH_SIZE = 69000000; // LiveJournal
//		}
//		int[] winSizes = {32*1024};
//		int[] winSizes = {2,2,2,4,8,16,32,64,128,256,512,1024,1024*2,1024*4,1024*8};
//		int[] winSizes = {1};
//		int[] winSizes = {1};
//		if (args.length>0) {
//			winSizes[0] = Integer.valueOf(args[0]);
//		}
//		int[] partitions = {32};
//		int[] partitions = {2,4,8,16,32,64};
//		int[] partitions = {4,8,16,32};
//		int mem_size = 1000; // the number of vertices in the memory
//		int[] latencies = {1,2,4,8}; //{8}; // times single edge latency
//		int singleEdgeLatency = 4 * 1000; //www_combined ms for the single edge streaming partitioning
//		int singleEdgeLatency = 1000 * 700; //brain
//		int singleEdgeLatency = 1000 * 1; // roadnet CA
//		int singleEdgeLatency = 1000 * 500; // livejournal
//		int singleEdgeLatency = 1000 * 3600; // twitterL
//		int singleEdgeLatency = 1000 * 11; // webGoogle
		
//		/*
//		 * ADWISE: varying L
//		 */
//		String[] params = new String[3];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-ADWISEstrategy";
////		outputPath = "Z:\\Curium_HGraphJ\\graphs\\www\\";
////		outputPath = "Z:\\Curium_HGraphJ\\graphs\\liveJournal\\";
////		outputPath = "Z:\\Curium_HGraphJ\\graphs\\roadNetCA\\";
////		outputPath = "Z:\\GraphDataSet\\TwitterL\\56Partitions\\";
////		outputPath = "Z:\\GraphDataSet\\twitter\\32Partitions\\";
//		String outputPath = "Z:\\GraphDataSet\\BrainNetwork\\32Partitions_3Threads\\";
////		outputPath = "Z:\\GraphDataSet\\BrainNetwork\\32Partitions_1Thread\\";
////		outputPath = "Z:\\GraphDataSet\\googleWeb\\32Partitions_3Thread\\";
//		Configuration.NUM_THREADS = 3;
//
//		for (int i=0; i<latencies.length; i++) {
//			Configuration.OUTPUT_PATH = outputPath + latencies[i] + "\\";
//			Configuration.MAXIMAL_LATENCY = Math.round(latencies[i] * singleEdgeLatency); // ms
//			Configuration.NUMBER_OF_PARTITIONS = partitions[0];
//			Configuration.MINIMAL_WINDOW_SIZE = (int) Math.round(latencies[i] * latencies[i] * latencies[i]);
//			Configuration.WINDOWSTRATEGY_BETA = 1.5;
//			Configuration.WINDOWSTRATEGY_GAMMA = 1;
//			Configuration.DYNAMIC_BETA = true;
//			Configuration.ADAPTIVE = true;
//			Configuration.LAZY = true;
//			Launcher.main(params);
//		}
		
		
//		/*
//		 * Window: parameters
//		 */
//		String[] params = new String[3];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-ADWISEstrategy";
//		Configuration.NUMBER_OF_PARTITIONS = 8;
////		Configuration.WINDOW_SIZE = 2000;
//		double[][] paramCombo = new double[][]{{0.1,0,0},{0.2,0,0},{0.3,0,0},{0.4,0,0},
//				{0.5,0,0},{0.6,0,0},{0.1,0,0},{0.1,0,0},{0.1,0,0},{0.1,0,0}};
//		for (double[] p : paramCombo) {
//			Configuration.WINDOW_SIZE = 1;
//			Configuration.MAXIMAL_LATENCY = 10 * 1000; // ms
//			Configuration.WINDOWSTRATEGY_ALPHA = p[0];
//			Configuration.WINDOWSTRATEGY_GAMMA = p[1];
//			Configuration.WINDOWSTRATEGY_DELTA = p[2];
//			Launcher.main(params);
//		}
		
//		/*
//		 * Correlation parameters <--> rep degree
//		 */
//		String[] params = new String[3];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-ADWISEstrategy";
//		Configuration.NUMBER_OF_PARTITIONS = 2;
//		Configuration.MAXIMAL_LATENCY = 10 * 1000; // ms
//		Configuration.WINDOW_SIZE = 512;
//		for (double i=0; i<=1; i+=0.1) {
////			Configuration.WINDOWSTRATEGY_ALPHA = i;
//			Configuration.WINDOWSTRATEGY_GAMMA = 0.2;
//			Configuration.MINIMAL_WINDOW_SIZE = 512;
//			Configuration.WINDOW_SIZE = Configuration.MINIMAL_WINDOW_SIZE;
//			Launcher.main(params);
//		}
		
//		/*
//		 * WISE: partitions
//		 */
//		String[] params = new String[3];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-ADWISEstrategy";
//		
//		for (int i=0; i<partitions.length; i++) {
//			Configuration.MAXIMAL_LATENCY = 100 * 1000; // + partitions[i] * 1000; // ms
//			Configuration.NUMBER_OF_PARTITIONS = partitions[i];
//			Configuration.MINIMAL_WINDOW_SIZE = 60;
//			Configuration.WINDOW_SIZE = Configuration.MINIMAL_WINDOW_SIZE;
//			Configuration.WINDOWSTRATEGY_BETA = 1.5;
//			Configuration.WINDOWSTRATEGY_GAMMA = 1;
//			Configuration.DYNAMIC_BETA = true;
//			Configuration.ADAPTIVE = true;
//			Configuration.LAZY = true;
////			Configuration.WINDOWSTRATEGY_GAMMA = 0;
//			Launcher.main(params);
////			Degree.storeToFile();
//		}
		
//		/*
//		 * Window: latency bounds / Tradeoff
//		 */
//		String[] params = new String[3];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-ADWISEstrategy";
////		int[] bounds = new int[]{1, 20, 40, 80, 160, 320 }; // sec
//		int[] bounds = new int[]{1, 2, 4, 8, 16, 32, 64 }; // times single-edge-latency
////		int[] bounds = new int[]{ 1 };
//		int singleEdgeLat = 1; // sec
//		int[] minW = new int[]{1, 4, 16, 64, 256, 1024, 4*1024 };
////		int[] minW = new int[]{1 };
////		int[] bounds = new int[]{20, 320 }; // sec
//		for (int i=0; i<bounds.length; i++) {
//			for (int j=0; j<3; j++) {
//				Configuration.WINDOW_SIZE = minW[i];
//				Configuration.MINIMAL_WINDOW_SIZE = Configuration.WINDOW_SIZE;
//				Configuration.MAXIMAL_LATENCY = bounds[i] * 1000 * singleEdgeLat; // ms
//				Configuration.NUMBER_OF_PARTITIONS = 8;
//				Launcher.main(params);
//			}
//		}
		
//		/*
//		 * DBH
//		 */
//		String[] params = new String[5];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-p";
//		params[3] = "" + partitions;
//		params[4] = "-DBH";
//		
//		for (int i=0; i<partitions.length; i++) {
//			params[3] = partitions[i] + "";
//			Launcher.main(params);
//		}
		
//		/*
//		 * HDRF
//		 */
//		double lambda = 1.1;
//		String[] params = new String[7];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-p";
//		params[3] = "" + partitions;
//		params[4] = "-hdrf";
//		params[5] = "" + lambda;
//		params[6] = "0.0001";
////		Configuration.OUTPUT_PATH = "Z:\\GraphDataSet\\TwitterBillion\\output\\";
////		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\liveJournal\\HDRF\\";
////		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\dummy\\";
////		Configuration.OUTPUT_PATH = "Z:\\GraphDataSet\\TwitterL\\56Partitions\\";
////		Configuration.OUTPUT_PATH = "Z:\\GraphDataSet\\RoadNetCA\\56Partitions\\";
////		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\twitter\\HDRF\\";
//		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\webGoogle\\HDRF\\";
//		for (int i=0; i<partitions.length; i++) {
//			//params[5] = "" + window;
//			params[3] = partitions[i] + "";
//			Launcher.main(params);
//		}
		
//		/*
//		 * Degree
//		 */
//		String[] params = new String[5];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-p";
//		params[3] = "" + partitions;
//		params[4] = "-degree";
//		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\webGoogle\\HDRF\\";
////		Configuration.OUTPUT_PATH = 
////				"Z:\\GraphDataSet\\LiveJournal\\56Partitions\\Degree\\";
////		Configuration.OUTPUT_PATH = "Z:\\GraphDataSet\\RoadNetCA\\56Partitions\\Degree\\";
//		Configuration.OUTPUT_PATH = "Z:\\Curium_HGraphJ\\graphs\\dummy\\";
//		for (int i=0; i<partitions.length; i++) {
//			//params[5] = "" + window;
//			params[3] = partitions[i] + "";
//			Launcher.main(params);
//		}
		
//		/*
//		 * Whisker
//		 */
//		String[] params = new String[5];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-p";
//		params[3] = "" + partitions;
//		params[4] = "-whisker";
//		
//		for (int i=0; i<partitions.length; i++) {
//			//params[5] = "" + window;
//			params[3] = partitions[i] + "";
//			Launcher.main(params);
//		}		
		
//		/*
//		 * Hashing
//		 */
//		String[] params = new String[5];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-p";
//		params[3] = "" + partitions;
//		params[4] = "-hashing";
//		
//		for (int i=0; i<partitions.length; i++) {
//			//params[5] = "" + window;
//			params[3] = partitions[i] + "";
//			Launcher.main(params);
//		}
		
//		/*
//		 * Window: unlimited memory
//		 */
//		String[] params = new String[5];
//		params[0] = "-in";
//		params[1] = graph;
//		params[2] = "-windowstrategy";
//		params[3] = "-memsize";
//		params[4] = "" + 2000;
//		Configuration.NUMBER_OF_PARTITIONS = 2;
//		Configuration.WINDOW_SIZE = 500;
//		
//		for (int i=0; i<partitions.length; i++) {
//			Configuration.NUMBER_OF_PARTITIONS = partitions[i];
//			Launcher.main(params);
//		}

//  }
	
//	private static void normalizeParams() {
//		double sum = Configuration.WINDOWSTRATEGY_ALPHA +
//				Configuration.WINDOWSTRATEGY_BETA +
//				Configuration.WINDOWSTRATEGY_GAMMA +
//				Configuration.WINDOWSTRATEGY_DELTA;
//		Configuration.WINDOWSTRATEGY_ALPHA /= sum;
//		Configuration.WINDOWSTRATEGY_BETA /= sum;
//		Configuration.WINDOWSTRATEGY_GAMMA /= sum;
//		Configuration.WINDOWSTRATEGY_DELTA /= sum;
//	}

//	private static void executeStrategy(String[] args) {
//		String graph = args[0];
//		int winSize = Integer.valueOf(args[1]);
//		int partitions = Integer.valueOf(args[2]);
//		String strategy = args[3];
//		int maxLatency = Integer.valueOf(args[4]);
////		System.out.println(args[0] + "--" + args[1] + "--" + args[2] + "--" + args[3]
////				+ "--" + args[4]);
//		String[] params;
//		Configuration.NUM_THREADS = 10;
//		Configuration.WRITE_TO_FILE = true;
//		Configuration.OUTPUT_PATH = strategy + "/";
//		Configuration.GRAPH = graph.split("/")[graph.split("/").length-1];
//		System.out.println(Configuration.OUTPUT_PATH);
//		Configuration.ESTIMATED_GRAPH_SIZE = Integer.valueOf(args[5]);
//		switch (strategy) {
//		case "Hash":
//			params = new String[5];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-p";
//			params[3] = "" + partitions;
//			params[4] = "-hashing";
//			Launcher.main(params);
//			break;
//		case "DBH":
//			params = new String[5];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-p";
//			params[3] = "" + partitions;
//			params[4] = "-DBH";
//			Launcher.main(params);
//			break;
//		case "HDRF":
//			double lambda = 1.1;
//			params = new String[7];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-p";
//			params[3] = "" + partitions;
//			params[4] = "-hdrf";
//			params[5] = "" + lambda;
//			params[6] = "0.0001";
//			Launcher.main(params);
//			break;
//		case "Degree":
//			params = new String[5];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-p";
//			params[3] = "" + partitions;
//			params[4] = "-degree";
//			Launcher.main(params);
//			break;
//		case "ADWISE1":
//			params = new String[3];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-ADWISEstrategy";
//			Configuration.MAXIMAL_LATENCY = maxLatency; // ms
//			Configuration.NUMBER_OF_PARTITIONS = partitions;
//			Configuration.WINDOWSTRATEGY_BETA = 1.5;	// beta is selected automatically
//			Configuration.WINDOWSTRATEGY_GAMMA = 1;
//			Configuration.MINIMAL_WINDOW_SIZE = (int) Math.round(1*1*1);
//			Configuration.DYNAMIC_BETA = true;
//			Configuration.ADAPTIVE = true;
//			Launcher.main(params);
//			break;
//		case "ADWISE2":
//			params = new String[3];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-ADWISEstrategy";
//			Configuration.MAXIMAL_LATENCY = 2*maxLatency; // ms
//			Configuration.NUMBER_OF_PARTITIONS = partitions;
//			Configuration.WINDOWSTRATEGY_BETA = 1.5;	// beta is selected automatically
//			Configuration.WINDOWSTRATEGY_GAMMA = 1;
//			Configuration.MINIMAL_WINDOW_SIZE = (int) Math.round(2*2*2);
//			Configuration.DYNAMIC_BETA = true;
//			Configuration.ADAPTIVE = true;
//			Launcher.main(params);
//			break;
//		case "ADWISE3":
//			params = new String[3];
//			params[0] = "-in";
//			params[1] = graph;
//			params[2] = "-ADWISEstrategy";
//			Configuration.MAXIMAL_LATENCY = 3*maxLatency; // ms
//			Configuration.NUMBER_OF_PARTITIONS = partitions;
//			Configuration.WINDOWSTRATEGY_BETA = 1.5;	// beta is selected automatically
//			Configuration.WINDOWSTRATEGY_GAMMA = 1;
//			Configuration.MINIMAL_WINDOW_SIZE = (int) Math.round(3*3*3);
//			Configuration.DYNAMIC_BETA = true;
//			Configuration.ADAPTIVE = true;
//			Launcher.main(params);
//			break;
//		}
//	}
	
}
