package adwise;

import strategies.PartitioningStrategy;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 */
public interface WindowBasedPartitioningStrategy extends PartitioningStrategy {
	public AssignedEdge getPartitionForEdge();

	public EdgeWindow getWindow();

}
