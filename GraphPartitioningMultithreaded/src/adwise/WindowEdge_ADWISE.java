package adwise;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import memory.Memory;
import model.Edge;
import util.Config;

public class WindowEdge_ADWISE extends Edge {
	public double[] scores;	// scores for all partitions
	public double maxScore = 0;
	public int maxScorePartitionID = 0;

	public int[] replicaScores;
	public int maxRepScore;
	
	public WindowEdge_ADWISE(Edge e, int k) {
		super(e.getFirstVertex(), e.getSecondVertex());
		replicaScores = new int[k];
		scores = new double[k];

	}

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		// edges
		Edge otherEdge = (Edge) other;
		return otherEdge.compareTo(this) == 0;
	}
	
	/**
	 * Initializes the replication scores for each partition
	 * @param memory
	 * @param numberOfPartitions
	 */
	public void updateRepScores(Memory memory, int numberOfPartitions) {
		replicaScores = new int[numberOfPartitions];
		int u = this.getFirstVertex();
		int v = this.getSecondVertex();
		this.maxRepScore = 0;
		Set<Integer> partitions_u = memory.getPartitionIdsOfVertex(u);
		Set<Integer> partitions_v = memory.getPartitionIdsOfVertex(v);
		
		
		for (int p_id = 0; p_id<numberOfPartitions; p_id++) {
			if (replicaScores[p_id]<2) { // otherwise skip new score computation				
				// replication score
				int repScore = 0;
				if (partitions_u.contains(p_id)) {
					repScore++;
				}
				if (partitions_v.contains(p_id)) {
					repScore++;
				}
				replicaScores[p_id] = repScore;
				if (repScore>maxRepScore) {
					this.maxRepScore = repScore;
				}
			}
		}
	}
	
	public void updateScores(Memory memory, Config config,
			ArrayList<Double> balanceScores, WindowStrategy_ADWISE strategy) {
		
		// we assume rep scores of all edges are already up to date
		
		maxScore = -1;

		// specifity score (does not change for each partition)
//		double specificityScore = getSpecifity();
		
		// calculate relative degree
		int v1 = this.getFirstVertex();
		int v2 = this.getSecondVertex();
		int deg_v1 = memory.getDegree().deg(v1,1);
		int deg_v2 = memory.getDegree().deg(v2,1);
		double rel_deg_v1 = (double) deg_v1 / (double) (deg_v1+deg_v2);
		double rel_deg_v2 = (double) deg_v2 / (double) (deg_v1+deg_v2);
//		double rel_deg_v1 = (double) deg_v1 / (double) (2*memory.getDegree().getMaxDegree());
//		double rel_deg_v2 = (double) deg_v2 / (double) (2*memory.getDegree().getMaxDegree());
		
		

		for (int p_id : config.getAllowedPartitions()) {
			
			// degree score (does not change for each partition
			double hdrfRepScore = hdrfRepScore(p_id, memory, v1, v2, rel_deg_v1, rel_deg_v2);
			
			// calculate score
//			double scoreREP = calculateScoreREP(edge, partitionId);
//			double scoreBAL = calculateScoreBAL(partitionId);
			
			// balance score (already multiplied with beta)
			double balanceScore = balanceScores.get(p_id);
			
			// degree score
//			double degreeScore = getDegreeScore(v1,v2);
			
			// clustering score
//			double clusteringScore = 0;
			double clusteringScore = getClusteringScore(memory, p_id, 
					((Window_ADWISE)strategy.getWindow()).getEdges());
//			System.out.println("Clustering score: " + clusteringScore);
			
			// total score
//			double score = hdrfRepScore + balanceScore * (1.8-specificityScore);
//			double score = hdrfRepScore + balanceScore 
//					+ config.getSpecificityWeight() * specificityScore;
//					+ Configuration.WINDOWSTRATEGY_GAMMA * specificityScore;
//					+ Configuration.WINDOWSTRATEGY_DELTA * degreeScore;
//			double score = hdrfRepScore + balanceScore + prios[p_id];
			double score = hdrfRepScore + balanceScore; // + clusteringScore;
//					+ config.getSpecificityWeight() * specificityScore;
			
			
//			if (balanceScore>hdrfRepScore + clusteringScore) {
//				System.out.println("Partitioning decision dominated by balancing!");
//			}
			
			// reinforce high-degree vertex splits
//			double degreeThreshold = Math.max(5, memory.getDegree().getMaxDegree()*0.9);
////			System.out.println("Max degree: " + degreeThreshold);
//			if (deg_v1>degreeThreshold) {
////				System.out.println("ssssssssssss");
//				if (memory.contains(v1, p_id)) {
//					score = Math.max(score-1, 0);
//				} else {
//					score += 2;
//				}
//			}
//			if (deg_v2>degreeThreshold) {
////				System.out.println("xxxxxxxxxxxxx");
//				if (memory.contains(v2, p_id)) {
//					score = Math.max(score-1, 0);
//				} else {
//					score += 2;
//				}
//			}
			
//			int threshold = 2;
//			if (deg_v1<threshold || deg_v1<threshold) {
//				score = 2*hdrfRepScore;
//			}

//			System.out.println("Total score: " + score);
//			System.out.println("HDRF rep score: " + hdrfRepScore);
//			System.out.println("Bal score: " + balanceScore);
//			System.out.println("Specificity: " + specificityScore);
//			System.out.println("Deg_v1: " + deg_v1);
//			System.out.println("Deg_v2: " + deg_v2);
//			System.out.println("Degree: " + degreeScore);
//			System.out.println();
			scores[p_id] = score;
			if (score>maxScore) {
				maxScore = score;
				maxScorePartitionID = p_id;
//				System.out.println(balanceScore + "\t\t" + hdrfRepScore + "\t\t" + maxScore + "\t\t" + maxRepScore);
			}
		}
	}
	
	private double getClusteringScore(Memory memory, int p_id, List<Edge> window) {
		if (window.size()==1) {
			return 0;
		}
		double clusteringScore = 0;
		int c = 0;
		for (Edge edge : window) {
			if (isNeighbor(edge, this)) {
				clusteringScore += ((WindowEdge_ADWISE)edge).replicaScores[p_id];
				c++;
			}
		}
		return clusteringScore/(double)(0.001+c);
//		return clusteringScore / (double) ((c+0.001)*2);
	}

	private boolean isNeighbor(Edge edge, Edge we) {
		if (edge.getFirstVertex()==we.getFirstVertex() &&
				edge.getSecondVertex()==we.getSecondVertex()) {
			return false;
		}
		return edge.getFirstVertex()==we.getFirstVertex() ||
				edge.getFirstVertex()==we.getSecondVertex() ||
				edge.getSecondVertex()==we.getFirstVertex() ||
				edge.getSecondVertex()==we.getSecondVertex();
	}

//	private Set<Integer> getAllowedPartitions(int v_id, int k) {
//		int val = v_id % k;
//		Set<Integer> par_set = new HashSet<Integer>();
//		if (val>v_id*0.5) {
//			for (int i=(int) Math.round(k*0.5); i<k; i++) {
//				par_set.add(i);
//			}
//		} else {
//			for (int i=0; i<(int) Math.round(k*0.5); i++) {
//				par_set.add(i);
//			}
//		}
//		return par_set;
//	}
	

//	private double getSpecifity(){
//		float sum = 0.0f;
//		for(int i = 0; i< replicaScores.length; i++){
//			sum += Math.abs(this.maxRepScore - replicaScores[i]);	
//		}
//		sum = sum/(float) (2*(replicaScores.length-1));
//		return sum;
//	}
	
//	/**
//	 * The larger the summed degree of v1 and v2, the smaller the score, the
//	 * more we focus on placing small degree edges optimally.
//	 * @param v1
//	 * @param v2
//	 * @return
//	 */
//	private double getDegreeScore(int v1, int v2, Memory memory) {
//		return 1-(double)(memory.getDegree().deg(v1, 1) + memory.getDegree().deg(v2, 1)) 
//				/ (double)(memory.getDegree().getMaxDegree() * 2);
//	}
	
	public double hdrfRepScore(int p_id, Memory memory,
			int v1, int v2, double rel_deg_v1, double rel_deg_v2) {
		double hdrfRepScore = 0;
//		System.out.println("Deg(v1): " + rel_deg_v1 + " deg(v2): " + rel_deg_v2);
		if (memory.contains(v1, p_id)) {
			// vertex v1 is replicated on p_id
			hdrfRepScore += 2-rel_deg_v1;
		}
		if (memory.contains(v2, p_id)) {
			// vertex v2 is replicated on p_id
			hdrfRepScore += 2-rel_deg_v2;
		}
		return hdrfRepScore;
	}
	
//	public double hdrfRepScore(int p_id, Memory memory,
//			int v1, int v2, double rel_deg_v1, double rel_deg_v2) {
//		double score = 0;
//		if (!isHighDegree(v1, memory) && memory.contains(v1, p_id)) {
//			score += 1;
//		}
//		if (!isHighDegree(v2, memory) && memory.contains(v2, p_id)) {
//			score += 1;
//		}
//		return score;
//	}
//
//	private boolean isHighDegree(int v, Memory memory) {
//		boolean highDeg = memory.getDegree().deg(v, 1)>memory.getDegree().getMaxDegree()*0.1;
////		if (highDeg) { System.out.println(v + "\t" + memory.getDegree().deg(v,1) + "\t" + memory.getDegree().getMaxDegree()); }
//		return highDeg;
//	}
	
}
