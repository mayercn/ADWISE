package adwise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import memory.Memory;
import util.Config;
import util.NamesOfStrategies;

/**
 * 
 * 
 * @author Rieger, Lukas
 * @author Laich, Larissa
 * @author Geppert, Heiko
 * @author Mayer, Christian
 * 
 */
public class WindowStrategy_ADWISE implements WindowBasedPartitioningStrategy {


	public Memory memory;
	private Config config;
	private Window_ADWISE window;

	public HashMap<String, Double> parameters = new HashMap<String, Double>();
	public ArrayList<Double> balances = new ArrayList<Double>();
	
	public int assignmentCounter;
	long timestamp;
	
	// Adaptive Window
	private int counter;
	private WindowStatistics_ADWISE stat;
	private long deadline; // in ns
	

	public WindowStrategy_ADWISE(Memory memory, Config config) {
		// this(memory,windowSize);
		this.memory = memory;
		this.config = config;
		this.window = new Window_ADWISE(this, config);

		for (int i = 0; i < config.getK(); i++) {
			this.balances.add(0.0d);
		}
		this.assignmentCounter = 0;
		this.counter = 0;
		this.stat = new WindowStatistics_ADWISE(config);
		this.deadline = System.nanoTime()
				+ (config.getMaxLatency()-1000) * 1000 * 1000; // 1 second puffer
//		this.allowedPartitions = new HashSet<Integer>();
//		for (int i=0; i<this.k; i++) {
//			if (i%config.getNumPartitioners()==config.getPartitioner_id()) {
//				// disjoint splitting of partition responsibilities
//				this.allowedPartitions.add(i);
//			}
//		}
//		System.out.println("Partitioner " + config.getPartitioner_id() + " has allowed partitions: " + this.allowedPartitions);
	}

	@Override
	public AssignedEdge getPartitionForEdge() {

//		window.trackNumberOfOptions(assignmentCounter);
//		if (assignmentCounter<=10000 && assignmentCounter>10000-100) {
//			window.trackScores(assignmentCounter);
//		}
//		window.updateAllScores(memory);
		
		// measure latency
		if (assignmentCounter==0) {
			timestamp = System.nanoTime();
		}
		long tmpLatency = System.nanoTime() - timestamp;
		this.timestamp = System.nanoTime();
		stat.updateStatistics(window.getSize(), tmpLatency);
		
		// update dynamic beta parameter
		if (config.isDynamicBeta() && assignmentCounter%50==0) {
			dynamicBeta();
		}

		// calculate new balancing scores
		// TODO: is this really good?
		if (assignmentCounter%30==0) {
			this.calculateBalances();
		}

		// get best edge in window
//		WindowEdge_ADWISE bestEdge = window.getAndRemoveBestEdge();
		AssignedEdge bestEdgeAssignment = window.getAndRemoveBestEdge();
//		if (bestEdge==null) {
//			return null;
//		}
		if (bestEdgeAssignment==null) {
			return null;
		}
		
		
//		int bestPartition;
//		if (bestEdge.maxRepScore==0) {
//			c1++;
//			System.out.println("DBH " + c1);
//			// bootstrap -> use DBH: if no info is available, assign edge towards low-deg vertex
//			int deg_u = memory.getDegree().deg(bestEdge.getFirstVertex(), 1);
//			int deg_v = memory.getDegree().deg(bestEdge.getSecondVertex(), 1);
//			if (deg_u<deg_v) {
//				bestPartition = bestEdge.getFirstVertex() % k;
//			} else {
//				bestPartition = bestEdge.getSecondVertex() % k;
//			}
////			System.out.println("Bootstrap: " + bestEdge + "\t" + bestPartition);
//		} else {
//			c2++;
//			System.out.println("ADWISE " + c2);
//			double bestScore = -1;
//			bestPartition = 0;
//			for (int p_id : config.getAllowedPartitions()) {
//				if (bestEdge.scores[p_id]>bestScore) {
//					bestScore = bestEdge.scores[p_id];
//					bestPartition = p_id;
//				}
//			}
//		}
		
		
//		AssignedEdge bestAssignment = new AssignedEdge(bestEdge, bestPartition);
		
		// update repScores of all edges that have changed && update candidates
		window.updateRepScores(
				(WindowEdge_ADWISE)bestEdgeAssignment.getEdge(),bestEdgeAssignment.getPartitionId());

		// track variables for adaptive window
		this.assignmentCounter++;
		this.counter++;
		
		// adapt window size
		if (config.isAdaptive() 
				&& counter==window.getSize()) {
			long maxLatency = calculateMaxLatencyPerEdgeAssignment();
//			System.out.println("Max latency (ns): " + maxLatency);
//			System.out.println("Latency needed (ns): " + tmpLatency);
//			System.out.println("Latency estimated (ns): " + stat.getAverageLatency(
//					Configuration.WINDOW_SIZE));
//			System.out.println("Old window size: " + Configuration.WINDOW_SIZE);
			if (maxLatency>0) {
				int newW = stat.getWindowSize(
						window.getSize(), maxLatency,
						assignmentCounter, window.getCandidateSize());
//				if (newW!=window.getSize()) {
//					System.out.println("New window size: " + newW);
//				}
				window.setSize(newW);
				
			} else {
				// deadline already missed: go go go!
				window.setSize(config.getW());
			}
			counter = 0;
		}
		
		return bestEdgeAssignment;
	}
	
	


	/**
	 * Returns the maximal latency allowed for assigning
	 * a single edge in nanoseconds.
	 * @return
	 */
	private long calculateMaxLatencyPerEdgeAssignment() {
		long edgesLeft = config.getGraphSize() - assignmentCounter;
		if (edgesLeft<1) {
			edgesLeft = 1;
		}
		double timeLeft = (double)(deadline-System.nanoTime()) 
				/ (double)edgesLeft; // nanosecs
		return Math.round(timeLeft);
	}

	/**
	 * Consider only allowed partitions
	 */
	private void calculateBalances() {

		double balScore;

		double min = Double.MAX_VALUE;
		double max = -1;
		for (int p_id : config.getAllowedPartitions()) {
			long tmpLoad = memory.getEdgeCount(p_id);
			min = Math.min(tmpLoad, min);
			max = Math.max(tmpLoad, max);			
		}

		for (int pid : config.getAllowedPartitions()) {
			balScore = 0.0;

			// bal Score is between 0 and 1 (if beta is 1)--- + 0.05 avoid division by 0 
			// value is higher if partition should get edges (greater distance to maximum)
			balScore = (float) (config.getBalancingWeight() * 
					(max - this.memory.getEdgeCount(pid)) 
					/ (max - min + 0.0001)); 
			this.balances.set(pid, balScore);
		}
	}


	@Override
	public EdgeWindow getWindow() {
		return this.window;
	}

	@Override
	public String toString() {
		return NamesOfStrategies.WINDOWSTRATEGY.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		return parameters;
	}

	/**
	 * Updates the beta parameter based on the imbalance of the partitions
	 * (considering only allowed partitions).
	 */
	private void dynamicBeta() {

		double min = Double.MAX_VALUE;
		double max = -1;
		for (int p_id : config.getAllowedPartitions()) {
			long tmpLoad = memory.getEdgeCount(p_id);
			min = Math.min(tmpLoad, min);
			max = Math.max(tmpLoad, max);			
		}
		double imbalance;
		if (max>0) {
			imbalance = (max-min)/max;
		} else {
			imbalance = 0;
		}
		//			System.out.println("Imbalance" + " " + imbalance);

		// tolerance as a function of the algorithm phase
		double tolerance = 1-(assignmentCounter
				/ (double) (config.getGraphSize()));
		tolerance = Math.max(tolerance,0.1);

		//			tolerance = 0.05; // fraction 

		// adapt beta within interval [0.1,0.5]
		config.setBalancingWeight(config.getBalancingWeight() + (imbalance - tolerance));
		double upper = 1.1;
		double lower = 0.2;
//		double upper = 5;
//		double lower = 0.6;
		if (config.getBalancingWeight()<lower) {
			config.setBalancingWeight(lower);
		}
		if (config.getBalancingWeight()>upper) {
			config.setBalancingWeight(upper);
		}
//		System.out.println(Configuration.WINDOWSTRATEGY_BETA);
	}

	public WindowStatistics_ADWISE getStat() {
		return stat;
	}
}
