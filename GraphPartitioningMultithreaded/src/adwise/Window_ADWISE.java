package adwise;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import memory.Memory;
import model.Edge;
import util.Config;

public class Window_ADWISE extends EdgeWindow {

	// the list of all edges in the window
	private List<WindowEdge_ADWISE> badEdges = new ArrayList<WindowEdge_ADWISE>();

	// only a few of them are candidates (those with high score)
	private ArrayList<WindowEdge_ADWISE> candidates = new ArrayList<WindowEdge_ADWISE>();


	private double exponentialAverageScore = 0;
	private double scoreThreshold = 0;	// gets updated
	private Config config;
	
	private WindowStrategy_ADWISE strategy;

	public Window_ADWISE(WindowStrategy_ADWISE strategy, Config config) {
		super(config.getW());
		this.config = config;
		this.strategy = strategy;
	}
	
	
	/**
	 * Update score threshold as exponential average
	 * of assigned edges.
	 * @param assignedScore
	 */
	private void updateScoreThreshold(double assignedScore) {
		exponentialAverageScore = 0.99 * exponentialAverageScore
				+ 0.01 * assignedScore;
		scoreThreshold = 0.1 + exponentialAverageScore;
		
//		System.out.println(candidates.size() + "\t" + badEdges.size() + "\t" + scoreThreshold);
//		System.out.println("Score threshold: " + scoreThreshold);
//		System.out.println("Best edge: " + assignedScore);
//		System.out.println("Candidate size: " + candidates.size());
//		System.out.println("Bad edges size: " + badEdges.size());
//		System.out.println("----------------");
	}
	
	@Override
	public boolean isEmpty() {
		return badEdges.isEmpty() && candidates.isEmpty();
	}

	@Override
	public boolean hasMoreEdges() {
		return isEmpty();
	}

	@Override
	public void setSize(int size) {
		super.setSize(size);
	}

	@Override
	public int getSize() {
		return super.getSize();
	}

	@Override
	public boolean hasCapacity() {
		return candidates.size()+badEdges.size()<size;
	}

	@Override
	public int getEdgeCount() {
		return super.getEdgeCount();
	}

	@Override
	public ArrayList<Edge> getEdges() {
		ArrayList<Edge> edges = new ArrayList<Edge>();
		edges.addAll(candidates);
		edges.addAll(badEdges);
		return edges;
	}

	@Override
	public boolean removeEdge(Edge e) {
		return true;
	}

	@Override
	public void addEdge(Edge e) {
		edges.add(e);
		
		strategy.memory.getDegree().updateDegreeDistribution(e);
		WindowEdge_ADWISE we = new WindowEdge_ADWISE(e, config.getK());
		we.updateRepScores(strategy.memory, config.getK());
		if (we.maxRepScore>0) {
			updateScore(we);
		}
		
		// update scoreThreshold for lazy approach
		updateScoreThreshold(we.maxScore);
		
		
		if (this.addToCandidates(we)) {
			if (!candidates.add(we)) {
				System.err.println("Could not add edge " + we);
				System.exit(-1);
			}
		} else {
			if (!badEdges.add(we)) {
				System.err.println("Could not add edge " + we);
				System.exit(-1);
			}
		}
	}
	


	/**
	 * Returns the edge with the highest score from the candidate
	 * set. If the candidate set is empty, the secondary set is used.
	 * @return
	 */
	public AssignedEdge getAndRemoveBestEdge() {
		
//		System.out.println("Candidates: " + candidates.size());
//		System.out.println("      Threshold: " + scoreThreshold); 
		// ensure that candidates are not empty
		if (candidates.size()==0) {
			refreshCandidates();
		}
//		if (strategy.assignmentCounter%100000==0) {
//			trackScores(strategy.assignmentCounter);
//		}
//		if (strategy.assignmentCounter%10==0) {
//			WriteToFile.writeln(
//					Configuration.OUTPUT_PATH + "windowInfo.dat", true, 
//					strategy.assignmentCounter + "\t" + candidates.size() 
//					+ "\t" + badEdges.size());
//		}
//		System.out.println(" Candidates: " + candidates.size() 
//				+ " Halde: " + badEdges.size());
		
		// get best edge from candidates
		WindowEdge_ADWISE bestEdge = null;
		double bestScore = -1;
		int bestPartitionID = 0;
		int index = 0;
		int i = 0;
//		System.out.println();
//		System.out.println("Window:");
//		System.out.println(strategy.allowedPartitions);
		for (WindowEdge_ADWISE edge : candidates) {
			updateScore(edge);
//			System.out.print(edge.maxRepScore  + "\t");
			if (edge.maxScore>bestScore) {
				bestScore = edge.maxScore;
				bestPartitionID = edge.maxScorePartitionID;
				index = i;
			}
			i++;
		}
		bestEdge = candidates.remove(index);
		AssignedEdge assignment = new AssignedEdge(bestEdge, bestPartitionID);
//		System.out.println("Best edge " + bestEdge + " score=" + bestScore);
//		System.out.println("Candidates size: " + candidates.size());
//		System.out.println();
		return assignment;
	}
	
	/**
	 * Adds all edges from the badEdges to candidates
	 */
	private void refreshCandidates() {
		Iterator<WindowEdge_ADWISE> iter = badEdges.iterator();
		while (iter.hasNext()) {
			WindowEdge_ADWISE edge = iter.next();
			updateScore(edge);
			if (addToCandidates(edge)) {
				iter.remove();
				candidates.add(edge);
			}
		}
	}
	
	private void updateScore(WindowEdge_ADWISE we) {
		we.updateScores(strategy.memory, config,
				strategy.balances, strategy);
	}
	

	/**
	 * Updates the repScores in all relevant window edges
	 * @param bestAssignment
	 */
	public void updateRepScores(WindowEdge_ADWISE we, int p_id) {
		int u = we.getFirstVertex();
		int v = we.getSecondVertex();
		boolean newReplicaU = !strategy.memory.contains(u, p_id);
		boolean newReplicaV = !strategy.memory.contains(v, p_id);

		if (newReplicaU) {
			increaseRepScore(candidates,true, u, p_id);
			increaseRepScore(badEdges,false,u, p_id);
		}
		if (newReplicaV) {
			increaseRepScore(candidates,true,v, p_id);
			increaseRepScore(badEdges, false,v, p_id);
		}
	}
	
//	public void updateAllScores(Memory memory) {
//		for (WindowEdge_ADWISE we : candidates) {
//			we.updateRepScores(memory, config.getK());
//			updateScore(we);
//		}
//	}
//	
	/**
	 * Increases the repScore of all vertices in the edge collection l
	 * that are equal vertexID.
	 * @param l
	 * @param vertexID
	 */
	private void increaseRepScore(Collection<WindowEdge_ADWISE> l,
			boolean candidateList, 
			int vertexID, int p_id) {
		Iterator<WindowEdge_ADWISE> iter = l.iterator();
		while (iter.hasNext()) {
			WindowEdge_ADWISE lwe = iter.next();
			int lwe_u = lwe.getFirstVertex();
			int lwe_v = lwe.getSecondVertex();
			if (vertexID==lwe_u || vertexID==lwe_v) {
//				System.out.println("Edge " + lwe + " repScore=" + lwe.replicaScores[p_id]);
				lwe.replicaScores[p_id]++;
				if (lwe.replicaScores[p_id]>lwe.maxRepScore) {
					lwe.maxRepScore = lwe.replicaScores[p_id];
				}
				if (!candidateList) {
					updateScore(lwe);
					if (addToCandidates(lwe)) {
						iter.remove();
						candidates.add(lwe);
					}
				}
			}
		}
	}
	
	private boolean addToCandidates(WindowEdge_ADWISE we) {
		if (!config.isLazy()) {
			return true;
		} else {
			// lazy window strategy is used
			if (candidates.isEmpty()) {
				return true;
			}
			if (we.maxRepScore>0
					) {
				return true;
			}
			return false;
		}
	}
	
	public void trackNumberOfOptions(int assignmentCounter) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(config.getOut_prefix() + "_no_options.txt", true), "utf-8"))) {
			// count number of edges with high score in window
			int c1 = 0;
			int c2 = 0;
			for (WindowEdge_ADWISE e : candidates) {
				for (int repScore : e.replicaScores) {
					c1 += repScore;
					c2 ++;
				}
			}
			writer.write(assignmentCounter + "\t" + c2 + "\t" + c1 + "\n");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void trackScores(int assignmentCounter) {
			try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(config.getOut_prefix() 
							+ "_scores.txt", true), "utf-8"))) {
				double summedRepScore = 0;
				double summedScore = 0;
				for (WindowEdge_ADWISE we : candidates) {
					for (int p_id : config.getAllowedPartitions()) {
						summedRepScore += we.replicaScores[p_id];
						summedScore += we.scores[p_id];
					}
				}
				writer.write(assignmentCounter + "\t" + 
						summedRepScore + "\t" + summedScore 
						+ "\t" + (candidates.size() * config.getAllowedPartitions().size())
						+ "\n");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public int getCandidateSize() {
		return candidates.size();
	}
	
	public ArrayList<WindowEdge_ADWISE> getCandidates() {
		return candidates;
	}
	
}
