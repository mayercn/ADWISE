package evaluation;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import memory.Memory;
import util.Config;

public class GlobalEvaluation {

	Config config;
	
	long startTime;
	
	long noEdges;
	long maxEdges;
	long minEdges;
	
	double balance;
	double replicationDegree;
	long executionTime; //ms
	Memory memory;
	
	public GlobalEvaluation(Config config) {
		this.config = config;
		this.startTime = System.currentTimeMillis();
	}
	
	public void updateMemory(Memory mem) {
		
		this.memory = mem;
		
		// we have everything to do final evaluation
		noEdges = calculateNoEdges();
		replicationDegree = calculateReplicationDegree();
		balance = calculateBalance();
		executionTime = System.currentTimeMillis() - startTime;
		printOnConsole();
		printEvaluationToFile();
		
	}
	
	
	private long calculateNoEdges() {
		long noEdges = 0;
		for (int pid : config.getAllowedPartitions()) {
			noEdges += memory.getEdgeCount(pid);
		}
		return noEdges;
	}

	/**
	 * Returns the average deviation from the average partition load in %
	 * @return
	 */
	private double calculateBalance2() {
		
		double avgPartitionSize = (double)noEdges / (double)config.getAllowedPartitions().size();

		double sum = 0.0;

		for (int pid : config.getAllowedPartitions()) {
			long pid_edges = memory.getEdgeCount(pid);
			sum += Math.pow(pid_edges - avgPartitionSize, 2);
		}
		double result = (Math.sqrt(sum / config.getAllowedPartitions().size()) / avgPartitionSize) * 100;

		return result;
	}
	
	/**
	 * Calculates a more simple balance metric.
	 * @return
	 */
	private double calculateBalance() {
		

		maxEdges = -999999999;
		minEdges = Long.MAX_VALUE;

		for (int pid : config.getAllowedPartitions()) {
			long pid_edges = memory.getEdgeCount(pid);
			maxEdges = Math.max(maxEdges, pid_edges);
			minEdges = Math.min(minEdges, pid_edges);
		}
		double result = (float)(maxEdges-minEdges)/(float)maxEdges;

		return result;
	}

	/**
	 * TAkes all memories from the independent partitions and merges the vertex replica caches.
	 * Based on this the total replication degree is calculated.
	 * @return
	 */
	private double calculateReplicationDegree() {
		
		// determine all vertices
		Set<Integer> vertices = memory.getVertices();
		System.out.println("No. vertices: " + vertices.size());
		
		// determine total no replicas
		long noReplicas = 0;
		for (Integer vid : vertices) {
			
			// determine replicas of vertex vid
			Set<Integer> replicas = memory.getVertexReplicas(vid);
			
			noReplicas += replicas.size();
		}
		
		double replicationDegree = (double) noReplicas / (double) vertices.size();
		return replicationDegree;
	}

	/**
	 * Print evaluation on command line
	 */
	public void printOnConsole() {

		String text = 	"\n******************************start********************************\n" + 
						"*   strategy: " + config.getStrategyName() + "\n" +
						"*   no. edges: " + noEdges + "\n" + 
						"*   balance: "	+ balance + "\n" + 
						"*   replication degree: " + replicationDegree + "\n" +
						"*   execution time: " + executionTime + "ms\n";

		text += "******************************end**********************************\n";
		System.out.println(text);
	}
	
	/**
	 * print evaluation into file
	 */
	public void printEvaluationToFile() {

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(config.getOut_prefix()	+ "_evals.dat", true)))) {
			writer.write(
					config.getW() + "\t" 
					+ config.getK() + "\t"
					+ config.getStrategyName() + "\t"
//					+ Configuration.MAXIMAL_LATENCY + "\t"
//					+ Configuration.WINDOWSTRATEGY_ALPHA + "\t"
//					+ Configuration.WINDOWSTRATEGY_BETA + "\t"
//					+ Configuration.WINDOWSTRATEGY_GAMMA + "\t"
//					+ Configuration.WINDOWSTRATEGY_DELTA + "\t"
					+ replicationDegree + "\t"
					+ balance + "\t" + executionTime + "\t"
					+ maxEdges + "\t" + minEdges + "\n");
			writer.close();
		} catch (Exception e) {
			System.out.println("ERROR: Couldn't write evaluation to file");
			e.printStackTrace();
		}
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(config.getOut_prefix()	+ "out_" + config.getPartitioner_id() + "_memory.dat", true)))) {
			for (Integer v_id : memory.getVertices()) {
				writer.write(String.valueOf(v_id));
				for (Integer p_id : memory.getPartitionIdsOfVertex(v_id)) {
					writer.write("\t" + p_id);
				}
				writer.write("\n");
			}
			writer.close();
		} catch (Exception e) {
			System.out.println("ERROR 2: Couldn't write evaluation to file");
			e.printStackTrace();
		}
	}
}
