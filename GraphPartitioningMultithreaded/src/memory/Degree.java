package memory;


import gnu.trove.map.hash.TIntIntHashMap;
import model.Edge;

/**
 * Class to store and maintain the degree table during processing
 * @author mayercn
 *
 */
public class Degree {

	private TIntIntHashMap degreeTable =
			new TIntIntHashMap();
	private int maxDegree = 1;
	
	/**
	 * Returns the estimated degree of vertex with v_id or the default value
	 * if the key v_id is not present in the table.
	 * @param v_id
	 */
	public Integer deg(Integer v_id, int defaultDegree) {
		Integer val = degreeTable.get(v_id);
		return (val==null) ? defaultDegree : val;
	}
	
	
	/**
	 * Increments the degree for vertex v_id by 1.
	 * @param v_id
	 */
	private void increment(Integer v_id) {
		Integer val = degreeTable.get(v_id);
		degreeTable.put(v_id, val!=null ? val+1 : 1);
		if (val>maxDegree) {
			maxDegree = val;
		}
	}
	
	/**
	 * Updates the degree for both incident vertices.
	 * @param e
	 */
	public void updateDegreeDistribution(Edge e) {
		increment(e.getFirstVertex());
		increment(e.getSecondVertex());
//		
//		
//		// update degree distribution
//		int v1 = e.getFirstVertex();
//		int v2 = e.getSecondVertex();
//		int degV1 = deg(v1, 0) + 1;
//		int degV2 = deg(v2, 0) + 1;
//		updateDegree(v1, degV1);
//		updateDegree(v2, degV2);
	}
	
	/**
	 * Returns the degree of the vertex with the largest degree that we have seen.
	 */
	public int getMaxDegree() {
		return maxDegree;
	}
	
//	public synchronized static void storeToFile() {
//		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
//				new FileOutputStream(Configuration.OUTPUT_PATH + "Degree.txt", true), "utf-8"))) {
//			for (Integer key : degreeTable.keySet()) {
//				writer.write(key + "\t" + degreeTable.get(key) + "\n");
//			}
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
