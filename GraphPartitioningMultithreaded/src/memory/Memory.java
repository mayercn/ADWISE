package memory;

import java.util.Set;
import model.Edge;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 */
public interface Memory {
	
	/**
	 * Returns the Degree object that maintaince Degree stuff
	 * @return
	 */
	public Degree getDegree();
	
	/**
	 * Returns the set of vertices in the memory object.
	 * @return
	 */
	public Set<Integer> getVertices();
	
	public Set<Integer> getVertexReplicas(int vid);
	

	/**
	 * Stores the edge on the indicated partition.
	 * 
	 * @param e
	 * @param partitionId
	 */
	public void store(Edge e, int partitionId);

	/**
	 * Returns all the partition ids of the partitions where the given vertex is
	 * contained in.
	 * 
	 * @param id
	 * @return Set of ids.
	 */
	public Set<Integer> getPartitionIdsOfVertex(int id);

	/**
	 * Returns true if vertexID is replicated on partitionID, else false;
	 * @param vertexID
	 * @param partitionID
	 * @return
	 */
	public boolean contains(int vertexID, int partitionID);


//	public int getLeastLoadedPartition();
	
	/**
	 * Returns the minimal load (i.e., number of edges) on any partition
	 * @return
	 */
	public long getMinLoad();
	
	/**
	 * Returns the maximal load (i.e., number of edges) on any partition
	 * @return
	 */
	public long getMaxLoad();
	
	/**
	 * Returns the average load (i.e., number of edges) on any partition
	 * @return
	 */
	public double getAvgLoad();

	/**
	 * Returns the edge count for the partition id
	 * @param pid
	 * @return
	 */
	public long getEdgeCount(int pid);

	/**
	 * Returns the replication degree, i.e., no. replicas divided by no. vertices
	 * @return
	 */
	public double getReplicationDegree();


}
