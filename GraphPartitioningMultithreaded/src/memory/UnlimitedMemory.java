package memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import model.Edge;
import util.Config;

/**
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 */
public class UnlimitedMemory implements Memory {
	private ArrayList<Integer> partitionEdgeCount;
	
//	private ConcurrentHashMap<Integer,TIntHashSet> A; // assignment v_id -> Set of partitions
	private HashMap<Integer,Set<Integer>> A; // assignment v_id -> Set of partitions
	
	private Degree degree;

	public UnlimitedMemory(Config config) {
		this.partitionEdgeCount = new ArrayList<Integer>();
		for (int i=0; i<config.getK(); i++) {
			this.partitionEdgeCount.add(0);
		}
		this.A = new HashMap<Integer,Set<Integer>>(1000000);
		this.degree = new Degree();
		
	}

	/**
	 * Stores the edge on the indicated partition.
	 * TODO: asynchronously via ConcurrentQueue...
	 * 
	 * @param e
	 * @param pid: the partition id
	 */
	@Override
	public void store(Edge e, int pid) {

		// thread-safe: add a new replica set for both vertices and update with partitionId
		Set<Integer> s1 = A.getOrDefault(e.getFirstVertex(), new HashSet<Integer>());
		A.putIfAbsent(e.getFirstVertex(), s1);
		s1.add(pid);

		Set<Integer> s2 = A.getOrDefault(e.getSecondVertex(), new HashSet<Integer>());
		A.putIfAbsent(e.getSecondVertex(), s2);
		s2.add(pid);
		
		// store edge on the partition it was assigned to
		this.partitionEdgeCount.set(pid, this.partitionEdgeCount.get(pid)+1);
		
	}

	/**
	 * Returns all the partition ids of the partitions where the given vertex is
	 * contained in.
	 * 
	 * @param id
	 * @return Set of ids.
	 */
	@Override
	public Set<Integer> getPartitionIdsOfVertex(int id) {
		
		return A.getOrDefault(id, new HashSet<Integer>());
	}


	@Override
	public long getMinLoad() {
		long minLoad = Long.MAX_VALUE;
		for (int i=0; i<partitionEdgeCount.size(); i++) {
			long load = partitionEdgeCount.get(i);
			if (load<minLoad) {
				minLoad = load;
			}
		}
		return minLoad;
	}

	@Override
	public long getMaxLoad() {
		long maxLoad = 0;
		for (int i=0; i<partitionEdgeCount.size(); i++) {
			long load = partitionEdgeCount.get(i);
			if (load>maxLoad) {
				maxLoad = load;
			}
		}
//		System.out.println("MaxLoad: " + maxLoad);
		return maxLoad;
	}
	
	@Override
	public double getAvgLoad() {
		double avgLoad = 0;
		for (int i=0; i<partitionEdgeCount.size(); i++) {
			avgLoad += partitionEdgeCount.get(i);
		}
		return avgLoad / (double) partitionEdgeCount.size();
	}

	@Override
	public boolean contains(int vertexID, int partitionID) {
		Set<Integer> ps = A.get(vertexID);
		if (ps==null || !ps.contains(partitionID)) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public long getEdgeCount(int pid) {
		return partitionEdgeCount.get(pid);
	}

	@Override
	public double getReplicationDegree() {
		long noReplicas = 0;
		long noVertices = A.size();
		for (Set<Integer> value : A.values()) {
            noReplicas += value.size();
        }
		return (double)noReplicas / (double)noVertices;
	}

	@Override
	public Degree getDegree() {
		return degree;
	}

	@Override
	public Set<Integer> getVertices() {
		return A.keySet();
	}
	
	@Override
	public Set<Integer> getVertexReplicas(int vid) {
		return A.get(vid);
	}
	
}
