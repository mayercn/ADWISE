package model;

import java.util.List;

import adwise.AssignedEdge;
import adwise.EdgeWindow;
import adwise.WindowBasedPartitioningStrategy;
import memory.Memory;
import strategies.PartitioningStrategy;
import strategies.SingleEdgePartitioningStrategy;
import util.Config;
import util.FileReaderAsynch;
import util.NamesOfStrategies;
import util.PartitionWriter;
import util.StrategyFactory;

/**
 * This class runs the partitioning strategy with all the constraints concerning
 * memory and number of partitions.
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 */

public class GraphPartitioner {

	private Memory memory;
	private Config config;
	
	private PartitionWriter[] partitionWriters; // one partitionWriter per partition
	private FileReaderAsynch inputReader;
	private PartitioningStrategy strategy;
	
//	private FileReader inputReader;
	
	private boolean isWindowBasedStrategy = false;
	private long progressStep;
	private long progressCounter = 0;

	/**
	 * Partitioner constructor
	 * 
	 * @param algorithm
	 * @param inputPath
	 * @param partitionsCount
	 * @param memSize
	 * @param edgeWriter
	 */
	public GraphPartitioner(Memory memory, Config config) {
		this.memory = memory;
		this.config = config;
		
		// create and start output edge writers - one per partition
		this.partitionWriters = 
				new PartitionWriter[config.getK()];
		for (int i=0; i<config.getK(); i++) {
			String filename = "out_p" + config.getPartitioner_id() + "_k" + i;
			this.partitionWriters[i] = new PartitionWriter(i, config.getOut_prefix(), filename);
//			this.partitionWriters[i].start();
		}

		// create and start input readers - one per input chunk
		this.inputReader = new FileReaderAsynch(config.getIn_filename() + config.getPartitioner_id());
		this.inputReader.start();
		
		this.strategy = StrategyFactory.instantiateStrategy(config, memory);
		
	}

	/**
	 * Runs the partitioning strategy on the given input graph. Sends every edge
	 * to the processing environment and starts the evaluation on the processing
	 * environment after finishing the partitioning.
	 */
	public void computePartitioning() {

		initProgressBar();

		Edge edge;
		AssignedEdge assignedEdge;
		int partitionId;

		WindowBasedPartitioningStrategy windowStrategy = null;
		SingleEdgePartitioningStrategy singleEdgeStrategy = null;
		EdgeWindow window = null;
		
		if (strategy instanceof WindowBasedPartitioningStrategy) {
			windowStrategy = (WindowBasedPartitioningStrategy) strategy;
			window = windowStrategy.getWindow();
			isWindowBasedStrategy = true;
		} else if (strategy instanceof SingleEdgePartitioningStrategy) {
			singleEdgeStrategy = (SingleEdgePartitioningStrategy) strategy;
			isWindowBasedStrategy = false;
		}
		
		if (isWindowBasedStrategy) {
			// window handling
			while (true) {
				List<Edge> newEdges = inputReader.fillWindow(window);
				if (window.isEmpty()) {
					break;
				}
				assignedEdge = windowStrategy.getPartitionForEdge();
//				System.out.println("Assigned Edge: " + assignedEdge);
				if (assignedEdge!=null) {
					partitionId = assignedEdge.getPartitionId();
					edge = assignedEdge.getEdge();
					memory.store(edge, partitionId);
					if (config.isWrite_to_file()) {
						partitionWriters[partitionId].writeEdge(edge);
					}
					progress();
				} else {
					System.err.println("Problem: there was a corrupt edge assignment...");
				}
			}
		} else {
			// single edge handling
			while (true) {
				edge = inputReader.getNextEdge();
				if (edge == null) {
					break;
				}
				// determine the partition for this edge with strategy
				partitionId = singleEdgeStrategy.getPartitionForEdge(edge);

				// Do not use Memory object for Hashing
				if (!NamesOfStrategies.HASH.toString().equals(config.getStrategyName())) {
					memory.store(edge, partitionId);
				}
				// this models the exterior system with its content
				if (config.isWrite_to_file()) {
					partitionWriters[partitionId].writeEdge(edge);
				}
				// life sign
				progress();
			}
		}
		
		// flush everything
		if (config.isWrite_to_file()) {
			for (int j=0; j<partitionWriters.length; j++) {
				partitionWriters[j].flush();
//				partitionWriters[j].writeEdge(null);
			}
		}
	}


	/**
	 * Calculates when the next step in the progressbar has to be set
	 */
	public synchronized void progress() {
		progressCounter++;
		if (progressCounter % progressStep == 0) {
			System.out.print("|");
		}
	}

	/**
	 * Estimates step of progress bar
	 */
	public void initProgressBar() {

		this.progressStep = config.getGraphSize() / 35;
		if (this.progressStep == 0) {
			this.progressStep = 1;
		}

		System.out.print("       0% |");
		for (int i = 0; i < 35; i++) {
			System.out.print(" ");
		}
		System.out.print("| 100% estimated\nProgress: ");
	}
	
	
}
