package strategies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import memory.Memory;
import model.Edge;
import util.Config;
import util.NamesOfStrategies;

/**
 * HighDegree (are) Replicated First Algorithm
 * 
 * @author Heiko Geppert
 *
 */
public class DBHStrategy implements SingleEdgePartitioningStrategy {

	private Memory memory;
	private Config config;
	private List<Integer> allowedPartitions;
	

	public DBHStrategy(Memory memory, Config config) {
		this.memory = memory;
		this.config = config;
		this.allowedPartitions = new ArrayList<Integer>(config.getAllowedPartitions());
	}
	
	/**
	 * Returns a simple hash function for vertex v_id
	 * @param v_id
	 * @return
	 */
	private int vertexHash(int v_id) {
		int index = v_id % config.getAllowedPartitions().size();
		return allowedPartitions.get(index);
//		return v_id % config.getK();
	}

	@Override
	public int getPartitionForEdge(Edge edge) {
		
		memory.getDegree().updateDegreeDistribution(edge);
		
		int u = edge.getFirstVertex();
		int v = edge.getSecondVertex();
		
		int deg_u = this.memory.getDegree().deg(u, 1);
		int deg_v = this.memory.getDegree().deg(v, 1);

		int partitionId;
		
		if (deg_u<deg_v) {
			partitionId = vertexHash(u);
		} else {
			partitionId = vertexHash(v);
		}
//		memory.store(edge, partitionId);
		return partitionId;
		
	}

//	/**
//	 * Adds the Nodes from the edge to the list of approximated node-degrees or
//	 * updates the degree if the node already has a value
//	 * 
//	 * @param edge
//	 */
//	private void updateApproximatedNodeDistribution(Edge edge) {
//		if (approximatedNodeDistribution.containsKey(edge.getFirstVertex())) {
//			approximatedNodeDistribution.put(edge.getFirstVertex(),
//					approximatedNodeDistribution.get(edge.getFirstVertex()) + 1);
//		} else {
//			approximatedNodeDistribution.put(edge.getFirstVertex(), 1);
//		}
//
//		if (approximatedNodeDistribution.containsKey(edge.getSecondVertex())) {
//			approximatedNodeDistribution.put(edge.getSecondVertex(),
//					approximatedNodeDistribution.get(edge.getSecondVertex()) + 1);
//		} else {
//			approximatedNodeDistribution.put(edge.getSecondVertex(), 1);
//		}
//	}


	@Override
	public String toString() {
		return NamesOfStrategies.DBH.toString();
	}
	
	@Override
	public HashMap<String, Double> getParameters() {
		HashMap<String, Double> parameters = new HashMap<String, Double>();
		return parameters;
	}
}
