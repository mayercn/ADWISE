package strategies;

import java.util.HashMap;

import memory.Memory;
import model.Edge;
import util.Config;
import util.NamesOfStrategies;

/**
 * HighDegree (are) Replicated First Algorithm
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 */
public class HDRFStrategy implements SingleEdgePartitioningStrategy {

//	private HashMap<Integer, Integer> approximatedNodeDistribution = new HashMap<Integer, Integer>();
	private double lambda;
	private double epsilon;
	private long minSize;
	private long maxSize;
	private Memory memory;
	private Config config;

	public HDRFStrategy(double lambda, double epsilon, Memory memory, Config config) {
		this.lambda = lambda;
		this.epsilon = epsilon;
		this.memory = memory;
		this.config = config;
	}


	@Override
	public int getPartitionForEdge(Edge edge) {

		memory.getDegree().updateDegreeDistribution(edge);
		updateMinMax();

		// Find partition with highest score
		int partitionId = calculatePartitionWithBestScore(edge);
		return partitionId;
	}

	/**
	 * calculates the HDRF Score for all partitions and returns the one with the
	 * best score
	 * 
	 * @param edge
	 * @param partitionSet
	 * @return
	 */
	private int calculatePartitionWithBestScore(Edge edge) {

		double maxScore = -1;
		double actualScore = -1;
		int maxPartition = 0;
		for (int partitionId : config.getAllowedPartitions()) {
			
			// calculate score
			double scoreREP = calculateScoreREP(edge, partitionId);
			double scoreBAL = calculateScoreBAL(partitionId);
			actualScore = scoreREP + scoreBAL;
			
			// is score max?
			if (actualScore > maxScore) {
				maxScore = actualScore;
				maxPartition = partitionId;
			}
		}
		// System.out.println("Max: " + maxScore);
		// System.out.println("---------");
		return maxPartition;
	}

	/**
	 * calculates the Balance Score of HDRF.
	 * 
	 * Formula: lambda * (maxSize-partitionSize)/(epsilon + maxSize - minSize)
	 * 
	 * @param partitionId
	 * @return
	 */
	private double calculateScoreBAL(int partitionId) {
		double temp1 = maxSize - memory.getEdgeCount(partitionId);
		double temp2 = epsilon + maxSize - minSize;
		double temp3 = lambda * temp1 / temp2;
		return Math.max(0, temp3);
	}

	/**
	 * calculates the Replication Score of HDRF
	 * 
	 * @param e
	 * @param partitionId
	 * @return
	 */
	private double calculateScoreREP(Edge e, int partitionId) {
		return g(e.getFirstVertex(), partitionId, e) + g(e.getSecondVertex(), partitionId, e);
	}

	/**
	 * g(v,p) used to calculate C-REP
	 * 
	 * @param v
	 *            Vertex ID
	 * @param p
	 *            Partition
	 * @param e
	 *            Edge where v is in
	 * @return
	 */
	private double g(int v, int partitionId, Edge e) {
		if (memory.contains(v, partitionId)) {
			return 1 + (1 - calculateTheta(e, v));
		} else {
			return 0;
		}
	}

	/**
	 * Theta of v
	 * 
	 * @param e
	 * @param v
	 *            the vertex Theta shall be calcuated
	 * @return
	 */
	private double calculateTheta(Edge e, int v) {
		int degreeV;
		int degreeOther;
		if (e.getFirstVertex() == v) {
			degreeV = memory.getDegree().deg(e.getFirstVertex(), 1);
			degreeOther = memory.getDegree().deg(e.getSecondVertex(), 1);
		} else {
			degreeV = memory.getDegree().deg(e.getSecondVertex(), 1);
			degreeOther = memory.getDegree().deg(e.getFirstVertex(), 1);
//			degreeV = approximatedNodeDistribution.get(e.getSecondVertex());
//			degreeOther = approximatedNodeDistribution.get(e.getFirstVertex());
		}
		return (double)degreeV / (double)(degreeV + degreeOther);
	}

	/**
	 * minsize & maxsize will be updated. Should be called before using min-
	 * /maxsize
	 */
	private void updateMinMax() {
		minSize = Integer.MAX_VALUE;
		maxSize = - 99999;
		
		for (Integer p_id : config.getAllowedPartitions()) {
			long load = this.memory.getEdgeCount(p_id);
			minSize = Math.min(minSize, load);
			maxSize = Math.max(maxSize, load);
		}
	}

	@Override
	public String toString() {
		return NamesOfStrategies.HDRF.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		HashMap<String, Double> parameters = new HashMap<String, Double>();
		parameters.put("lamda", this.lambda);
		parameters.put("epsilon", this.epsilon);
		return parameters;
	}
	
//	public static void main(String[] args) {
//		System.out.println("Res: " + (double)2/(double)(2+5));
//	}
}
