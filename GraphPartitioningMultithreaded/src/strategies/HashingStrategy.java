package strategies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import memory.Memory;
import model.Edge;
import util.Config;
import util.NamesOfStrategies;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 *         Primitive hash strategy for testing purposes.
 */
public class HashingStrategy implements SingleEdgePartitioningStrategy {
	private int counter = 0;
	private List<Integer> allowedPartitions;

	public HashingStrategy(Memory memory, Config config) {
		this.allowedPartitions = new ArrayList<Integer>(config.getAllowedPartitions());
	}

	/**
	 * Distribute edges over partitions evenly.
	 */
	@Override
	public int getPartitionForEdge(Edge edge) {
		this.counter++;
		this.counter = this.counter % this.allowedPartitions.size();
		return this.allowedPartitions.get(counter);
	}

	@Override
	public String toString() {
		return NamesOfStrategies.HASH.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		return new HashMap<String, Double>();
	}
}
