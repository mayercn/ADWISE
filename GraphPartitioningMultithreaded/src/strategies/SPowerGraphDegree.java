package strategies;

import java.util.HashMap;
import memory.Memory;
import model.Edge;
import util.Config;
import util.NamesOfStrategies;

/**
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 * 
 *         The Degree algorithm as described in the S-PowerGraph Paper
 *
 */
public class SPowerGraphDegree implements SingleEdgePartitioningStrategy {
	private Edge e;
	private double maxEdgeCount;
	private double minEdgeCount;
	private long currentPartitionsEdgeCount;
	private double averageEdgesPerPartition;
	// allowed imbalance between max/average nodes on partitions
	private final double epsilon = 0.1;
	private Memory memory;
	private Config config;

	public SPowerGraphDegree(Memory memory, Config config) {
		this.memory = memory;
		this.config = config;
	}

	/**
	 * Main method of the algorithm. This method implements the algorithm DEGREE
	 * as it is described in the S-PowerGraph paper. It returns for a given edge
	 * the optimal partition where to add the current edge in order to achieve
	 * optimal balance and replication.
	 */
	@Override
	public int getPartitionForEdge(Edge edge) {
		
		memory.getDegree().updateDegreeDistribution(edge);
		
		this.e = edge;
		double maxScore = 0.0;
		double currentScore = 0.0;
		double ratio;
		int bestPartitionId = 0;
		boolean onlyBalance = false;

		initializeIterationStep();

		ratio = ((double) maxEdgeCount / (double) averageEdgesPerPartition);
		onlyBalance = (ratio >= (1.0 + epsilon)) ? true : false;

		for (int partitionId : config.getAllowedPartitions()) {
			if (onlyBalance) {
				// only optimize balance
				currentScore = getBalance(partitionId);
			} else {
				// optimize overall score
				currentScore = getScoreOfPartition(partitionId);
			}

			if (currentScore > maxScore) {
				bestPartitionId = partitionId;
				maxScore = currentScore;
			}
		}
		return bestPartitionId;
	}

	/**
	 * This method returns the score of the given partition in the current state
	 * of the iteration.
	 * 
	 * @param partitionId
	 * @return
	 */
	private double getScoreOfPartition(int partitionId) {
		double score = 0.0;
		int u = e.getFirstVertex();
		int v = e.getSecondVertex();
		double degreeU;
		double degreeV;

		boolean uReplicated = memory.contains(u, partitionId);
		boolean vReplicated = memory.contains(v, partitionId);
		

		degreeU = memory.getDegree().deg(u, 1);
		degreeV =  memory.getDegree().deg(v, 1);

		if (uReplicated) {
			// partition already contains a replication of vertex u
			score += 1.0;
			if (degreeU <= degreeV) {
				// smaller degree
				score += 1.0;
			}
		}
		if (vReplicated) {
			// partition already contains a replication of vertex v
			score += 1.0;
			if (degreeV <= degreeU) {
				// smaller degree
				score += 1.0;
			}
		}
		
		score += getBalance(partitionId);
		return score;
	}

	/**
	 * Calculates the balance of the given partition. For the balance value we
	 * use the algorithm from the paper.
	 * 
	 * @param partitionId
	 * @return balance
	 */
	private double getBalance(int partitionId) {
		currentPartitionsEdgeCount = memory.getEdgeCount(partitionId);

		return ((double) (maxEdgeCount - currentPartitionsEdgeCount)) / ((double) (maxEdgeCount - minEdgeCount + 1.0));
	}

	/**
	 * Sets the global variables minEdgeCount, maxEdgeCount and
	 * averageEdgesPerPartition. These values have to be actualized after each
	 * assignment of an edge.
	 */
	private void initializeIterationStep() {

		minEdgeCount = Integer.MAX_VALUE;
		maxEdgeCount = - 99999;
		long tmp_sum = 0;
		
		for (Integer p_id : config.getAllowedPartitions()) {
			long load = this.memory.getEdgeCount(p_id);
			minEdgeCount = Math.min(minEdgeCount, load);
			maxEdgeCount = Math.max(maxEdgeCount, load);
			tmp_sum += load;
		}
		
		
		minEdgeCount = this.memory.getMinLoad();
		maxEdgeCount = this.memory.getMaxLoad();
		averageEdgesPerPartition = (double) tmp_sum / (double) config.getAllowedPartitions().size();
	}

//	/**
//	 * Estimates the degree of a given node. The degree is for indirected edges.
//	 * The method models a power-law distribution of vertex degrees.
//	 * 
//	 * @param vertex
//	 * @return estimated degree of vertex
//	 */
//	private double getDegreeEstimation(int vertex) {
//		double degree = degreeDistribution
//				.adjustOrPutValue(vertex, 1.0d, 1.0d);
//		
//		
//		
////		if (degreeDistribution.containsKey(vertex)) {
//////			degree = degreeDistribution.get(vertex);
////			degreeDistribution.get(vertex)++;
//////			degreeDistribution.put(vertex, degree);
////		} else {
////			degreeDistribution.put(vertex, 1.0);
////			degree = 1.0;
////		}
//		return degree;
//	}

	@Override
	public String toString() {
		return NamesOfStrategies.DEGREE.toString();
	}

	@Override
	public HashMap<String, Double> getParameters() {
		return new HashMap<String, Double>();

	}
}