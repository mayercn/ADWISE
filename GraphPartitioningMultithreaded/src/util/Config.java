package util;

import java.util.HashMap;
import java.util.HashSet;

/**
 * The config class serves as central unit for all parameters in the system.
 * Every independent partitioning thread has its independent config class.
 * @author mayercn
 *
 */
public class Config {

	/*
	 * Variables
	 */
	private int partitioner_id;
	private String in_filename;
	private String out_prefix; // e.g. facebook -> facebook_{partitioner_id}_{partition_id}
	private boolean write_to_file;
	private String strategyName;
	private long graphSize; // the estimated size of the graph
	private int k; // the number of partitions
	private HashSet<Integer> allowedPartitions;
	
	// HDRF
	private double lambda = 1.1;
	private double epsilon = 0.0001;
	
	// ADWISE
	private int w; // the minimal window size
	private long maxLatency; // the maximal latency for the whole partitioning task (ms)
	private boolean dynamicBeta;
	private boolean adaptive;
	private boolean lazy;
	private double specificityWeight = 0.0;
	private double balancingWeight = 1;
	

	/*
	 * Getter and Setters
	 */

	public double getSpecificityWeight() {
		return specificityWeight;
	}
	public void setSpecificityWeight(double specificityWeight) {
		this.specificityWeight = specificityWeight;
	}
	public double getLambda() {
		return lambda;
	}
	public void setLambda(double lambda) {
		this.lambda = lambda;
	}
	public double getEpsilon() {
		return epsilon;
	}
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}
	
	public int getPartitioner_id() {
		return partitioner_id;
	}
	public void setPartitioner_id(int partitioner_id) {
		this.partitioner_id = partitioner_id;
	}
	public String getIn_filename() {
		return in_filename;
	}
	public void setIn_filename(String in_filename) {
		this.in_filename = in_filename;
	}
	public String getOut_prefix() {
		return out_prefix;
	}
	public void setOut_prefix(String out_prefix) {
		this.out_prefix = out_prefix;
	}
	public boolean isWrite_to_file() {
		return write_to_file;
	}
	public void setWrite_to_file(boolean write_to_file) {
		this.write_to_file = write_to_file;
	}
	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}
	public int getW() {
		return w;
	}
	public void setW(int w) {
		this.w = w;
	}
	public long getMaxLatency() {
		return maxLatency;
	}
	public void setMaxLatency(long maxLatency) {
		this.maxLatency = maxLatency;
	}
	public long getGraphSize() {
		return graphSize;
	}
	public void setGraphSize(long graphSize) {
		this.graphSize = graphSize;
	}
	public boolean isDynamicBeta() {
		return dynamicBeta;
	}
	public void setDynamicBeta(boolean dynamicBeta) {
		this.dynamicBeta = dynamicBeta;
	}
	public boolean isAdaptive() {
		return adaptive;
	}
	public void setAdaptive(boolean adaptive) {
		this.adaptive = adaptive;
	}
	
	/**
	 * Sets the parameters from the program argument
	 * @param args
	 */
	public void setParams(String[] args) {
		if (args.length!=12) {
			System.out.println("Wrong usage of parameters.");
			System.out.println(	"	* 		args[0] - partitioner id" +
								"	* 		args[1] - inFile" + 
								"	* 		args[2] - outPrefix" + 
								"	* 		args[3] - writeToFile (true/false)" + 
								"	* 		args[4] - strategyName (hashing/hdrf/degree/DBH/ADWISE)" + 
								"	* 		args[5] - number of partitions" + 
								"	* 		args[6] - minimal window size (ignored by non-window strategies)" + 
								"	* 		args[7] - maximal Latency (sec) for the whole partitioning task (ignored by non-window strategies)" + 
								"	* 		args[8] - dynamic beta? (true/false)" + 
								"	* 		args[9] - adaptive? (true/false)" + 
								"	* 		args[10]- lazy? (true/false)" +
								"	*		args[11]- allowed partitions (e.g. 1,2,3)");
		} else {
			partitioner_id = Integer.valueOf(args[0]);
			in_filename = args[1];
			out_prefix = args[2];
			write_to_file = Boolean.valueOf(args[3]);
			strategyName = args[4];
			k = Integer.valueOf(args[5]);
			w = Integer.valueOf(args[6]);
			maxLatency = Long.valueOf(args[7]);
			dynamicBeta = Boolean.valueOf(args[8]);
			System.out.println("Dynamic Beta: " + dynamicBeta);
			adaptive = Boolean.valueOf(args[9]);
			lazy = Boolean.valueOf(args[10]);
			this.allowedPartitions = new HashSet<Integer>();
			for (String s : args[11].split(",")) {
				this.allowedPartitions.add(Integer.valueOf(s));
			}
		}
	}
	
	public Config copy() {
		Config newConfig = new Config();
		newConfig.setAdaptive(this.isAdaptive());
		newConfig.setBalancingWeight(this.getBalancingWeight());
		newConfig.setDynamicBeta(this.isDynamicBeta());
		newConfig.setEpsilon(this.getEpsilon());
		newConfig.setGraphSize(this.getGraphSize());
		newConfig.setIn_filename(this.getIn_filename());
		newConfig.setK(this.getK());
		newConfig.setLambda(this.getLambda());
		newConfig.setLazy(this.isLazy());
		newConfig.setMaxLatency(this.getMaxLatency());
		newConfig.setOut_prefix(this.getOut_prefix() + "");
		newConfig.setPartitioner_id(this.getPartitioner_id());
		newConfig.setSpecificityWeight(this.getSpecificityWeight());
		newConfig.setStrategyName(this.strategyName);
		newConfig.setW(w);
		newConfig.setWrite_to_file(write_to_file);
		return newConfig;
	}
	public boolean isLazy() {
		return lazy;
	}
	public void setLazy(boolean lazy) {
		this.lazy = lazy;
	}
	public double getBalancingWeight() {
		return balancingWeight;
	}
	public void setBalancingWeight(double balancingWeight) {
		this.balancingWeight = balancingWeight;
	}
	public HashSet<Integer> getAllowedPartitions() {
		return allowedPartitions;
	}
	public void setAllowedPartitions(HashSet<Integer> allowedPartitions) {
		this.allowedPartitions = allowedPartitions;
	}
	
}
