package util;

/**
 * Enum containing the strategy names of all available strategies.
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 *
 */
public enum NamesOfStrategies {

	HASH("Hash"), HDRF("HDRF"), DEGREE("Degree"), DBH("DBH"),
					ADWISE("ADWISE"), WINDOWSTRATEGY("windowstrategy");

	private final String stringValue;

	private NamesOfStrategies(String stringValue) {
		this.stringValue = stringValue;
	}

	public String toString() {
		return stringValue;
	}
}
