package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import model.Edge;

/**
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 * PartitionWriter contains the buffer which edges to be written in one
 * step asynchronously.
 */
public class PartitionWriter {

	int partitionId;
	String fileName;
	private BufferedWriter out;

	public PartitionWriter(int partitionId, String folder, String filePath) {
		this.partitionId = partitionId;
		this.fileName = filePath;
		try {
			new File(folder).mkdirs();
			this.out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(folder + fileName, false)), 1024*1024);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeEdge(Edge e) {
		try {
			out.write( e.toString() + "\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void flush() {
		try {
			this.out.flush();
			this.out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
