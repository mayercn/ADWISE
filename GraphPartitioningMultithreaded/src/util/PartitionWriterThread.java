package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import model.Edge;

/**
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 * PartitionWriter contains the buffer which edges to be written in one
 * step asynchronously.
 */
public class PartitionWriterThread extends Thread {

	// edge buffer for writing
	int partitionId;
	String fileName;
	private BufferedWriter out;
	private ArrayBlockingQueue<List<Edge>> buffer; // of edge batches
	int batchSize = 100;
	List<Edge> currentBatch;

	public PartitionWriterThread(int partitionId, String folder, String filePath) {
		this.partitionId = partitionId;
		this.fileName = filePath;
		this.buffer = new ArrayBlockingQueue<List<Edge>>(1000);
		this.currentBatch = new ArrayList<Edge>();
		try {
			new File(folder).mkdirs();
			this.out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(folder + fileName, false)), 1024*1024);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			List<Edge> edgeBatch;
			while ((edgeBatch=buffer.take())!=null) {
				for (Edge e : edgeBatch) {
					out.write( e.toString() + "\n");
				}
				out.flush();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			this.out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeEdge(Edge e) {
//		try {
//			buffer.put(e);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		try {
			if (currentBatch.size()<batchSize) {
				currentBatch.add(e);
			} else {
				buffer.put(currentBatch);
				currentBatch = new ArrayList<Edge>();
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void flush() {
//		try {
//			this.out.flush();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		try {
			buffer.put(currentBatch);
			currentBatch = new ArrayList<Edge>();
			this.out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
