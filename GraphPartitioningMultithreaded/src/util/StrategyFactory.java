package util;

import adwise.WindowStrategy_ADWISE;
import memory.Memory;
import strategies.DBHStrategy;
import strategies.HDRFStrategy;
import strategies.HashingStrategy;
import strategies.PartitioningStrategy;
import strategies.SPowerGraphDegree;

/**
 * FactoryClass which provides strategies
 * 
 * @author Christian Mayer
 * @author Heiko Geppert
 * @author Larissa Laich
 * @author Lukas Rieger
 *
 *
 */
public class StrategyFactory {

	/**
	 * Factory method for partitioning strategies. Used in the constructor
	 * 
	 * @param strategyName
	 *            Name from NamesOfStrategies Enum
	 * @param memory
	 *            the LimitedMemory used in the strategy
	 * @return object of chosen strategy
	 */
	public static PartitioningStrategy instantiateStrategy(
			Config config, Memory memory) {
		
		
		String strategyName = config.getStrategyName();

		if (NamesOfStrategies.HASH.toString().equals(strategyName)) {
			return new HashingStrategy(memory, config);
		} else if (NamesOfStrategies.HDRF.toString().equals(strategyName)) {
			return new HDRFStrategy(config.getLambda(), config.getEpsilon(), memory, config);
		} else if (NamesOfStrategies.DEGREE.toString().equals(strategyName)) {
			return new SPowerGraphDegree(memory, config);
		} else if (NamesOfStrategies.ADWISE.toString().equals(strategyName)) {
			return new WindowStrategy_ADWISE(memory, config);
		} else if (NamesOfStrategies.DBH.toString().equals(strategyName)) {
			return new DBHStrategy(memory, config);
		}
		System.err.println("Couldn't find strategy with name " + strategyName + ". Hashing will be used.");
		return new HashingStrategy(memory, config);
	}
}
