package util;

import java.io.File;

public class Util {

	/**
	 * Returns the estimated number of edges in a file based on the file size.
	 * @param filename
	 * @return
	 */
	public static long estimateNumEdges(String filename) {
		File file = new File(filename);
		long fileSize = file.length();
		long estimatedNumberOfEdges;
		double divisor = 6.1;
		long b = 0;

		// longer files mean more edges, more edges means longer node ids
		if (fileSize > 5000) {
			divisor = 7.5;
		}
		if (fileSize > 10000) {
			divisor = 7.7;
		}
		if (fileSize > 50000) {
			divisor = 8;
		}
		if (fileSize > 100000) {

		}
		if (fileSize > 200000) {
			divisor = 9;
		}
		if (fileSize > 500000) {
			divisor = 9.5;
		}
		if (fileSize > 700000) {
			divisor = 10;
		}
		if (fileSize > 1000000) {
			divisor = 1 / 0.0679;
			b = -16326;
		}

		estimatedNumberOfEdges = (long) (fileSize / divisor) + b;
		System.out.println("There are approx. " + estimatedNumberOfEdges + " edges in the file: " + filename);
		return estimatedNumberOfEdges;
	}
}
