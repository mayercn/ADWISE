package util;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class WriteToFile {
	private static HashMap<String, BufferedWriter> writers =
			new HashMap<String, BufferedWriter>();
	
	
	
	public static void write(String filename, boolean append, String s) {
		try {
			BufferedWriter out = writers.get(filename);
			if (out==null) {
				out = new BufferedWriter(
						new FileWriter(new File(filename), append), 1024*100);
				writers.put(filename, out);
			}
			out.write(s);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void writeln(String filename, boolean append, String s) {
		try {
			BufferedWriter out = writers.get(filename);
			if (out==null) {
				out = new BufferedWriter(
						new FileWriter(new File(filename), append));
				writers.put(filename, out);
			}
			out.write(s);
			out.newLine();
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
