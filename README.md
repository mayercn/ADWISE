HINTS:
- Start with "Main_ADWISE.java" and branch "GraphPartitioningMultithreaded"
- Use parameters as described there
- Configuration file: "Config.java"
- The evaluation package is depreciated (use your own scripts to analyze the replication degree and workload balance of the output graphs) 