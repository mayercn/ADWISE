file = "soc-LiveJournal1.txt"
outfile = "out" + file
comment = "#"

with open(file, mode='r') as infile:
    with open(outfile, mode="w") as out:
        for line in infile:
            if not line.startswith(comment):
                out.write(line)
                #print(line)
